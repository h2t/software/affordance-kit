AffordanceKit
===========================

Description
---

AffordanceKit is a C++ library for the representation, visual detection and validation of whole-body affordances in unknown environment.
The library is split into four essential parts which base upon each other, but can also be used individually:

- The `Affordance` class implements the formalization for whole-body affordances proposed in (Kaiser et al., 2018). Affordances are represented as Dempster-Shafer belief assignments distributed over the space of end-effector poses (in the case of a `UnimanualAffordance`) or the space of Kartesian products of two end-effector poses (in the case of a `Bimanual Affordance`).
For further information regarding the formalization of affordances implemented in AffordanceKit, refer to (Kaiser et al., 2018). See the full citation in the references section of this README.

- The class `PrimitiveExtractor` implements a RANSAC-based strategy to detecting geometric primitives in a point cloud (e.g. RGB-D).
The point cloud is expected to be pre-segmented and in the PCL XYZL format.
While different types of geometrics primitives are available, the extraction process will mainly find planar primitives.
The result of the primitive extraction process is a `PrimitiveSet` instance.

- Based on a scene description as geometric primitives, AffordanceKit is able to extract visual evidence for defined affordances.
It further implements mechanisms for evidence fusion and the hierarchical propagation of evidence, such that affordance-related evidence from different modalities (e.g. haptic validation experiments) can be considered appropriately.

- AffordanceKit implements an affordance hierarchy for the exemplary application of a humanoid robot operating in unknown environments.
Details on the hierarchical approach to affordance detection and validation implemented in AffordanceKit can be found in (Kaiser et al., 2016).

AffordanceKit is designed to be standalone.
However, the robot development environment [ArmarX](https://armarx.humanoids.kit.edu/) contains the library *AffordanceKitArmarX* which enriches the data types defined in *AffordanceKit* by ArmarX-specific memory import/export and visualization functionalities.

Dependencies and Requirements
---

For building and running AffordanceKit, the following dependencies are required:

- [Simox](https://gitlab.com/Simox/simox)
- Eigen3
- PCL
- OpenMP

AffordanceKit has been tested under Ubuntu 14.04 and Ubuntu 18.04. Other operating systems (particularly Windows and Mac OS) are not supported.


Installation
---

The source code of AffordanceKit can be accessed on [GitLab](https://gitlab.com/h2t/affordance-kit).
As of now, no binary packages are available for download.
For building AffordanceKit from source, pursue the following procedure:

```bash
git clone git@gitlab.com:h2t/affordance-kit.git
cd affordance-kit && mkdir build && cd build
cmake ..
make
```

For installing AffordanceKit, use the CMake variable `CMAKE_INSTALL_PREFIX`:

```bash
git clone git@gitlab.com:h2t/affordance-kit.git
cd affordance-kit && mkdir build && cd build
cmake -DCMAKE_INSTALL_PREFIX=/path/to/install/dir ..
make && make install
```


References
---

If you want to refer to AffordanceKit in a scientific article, please use one of the following references: 

```
@ARTICLE {Kaiser2018,
    author = {Peter Kaiser and Tamim Asfour},
    title = {Autonomous Detection and Experimental Validation of Affordances},
    pages = {1949--1956},
    volume = {3},
    number = {3},
    journal = {IEEE Robotics and Automation Letters (RA-L)},
    year = {2018},
}

@INCOLLECTION = {Kaiser2018b,
    author = {Peter Kaiser},
    publisher = {KIT Scientific Publishing},
    booktitle = {Karlsruhe Series on Humanoid Robotics},
    title = {Whole-Body Affordances for Humanoid Robots: A Computational Approach},
    year = {2018},
}

@INPROCEEDINGS {Kaiser2016,
    author = {Peter Kaiser and Eren E. Aksoy and Markus Grotz and Tamim Asfour},
    title = {Towards a Hierarchy of Loco-Manipulation Affordances},
    booktitle = {IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS)},
    pages = {2839--2846},
    year = {2016},
}    
```

Authors
---

- [Peter Kaiser](https://scholar.google.com/citations?user=wqVErWYAAAAJ&hl=en)
- [Markus Grotz](https://scholar.google.com/citations?user=ywTBxOkAAAAJ&hl=en)
- [Eren E. Aksoy](https://scholar.google.com/citations?user=2xCdZQcAAAAJ&hl=en)
- [Tamim Asfour](https://scholar.google.com/citations?user=65bIT4oAAAAJ&hl=en)

License
---

AffordanceKit is released under the GNU Lesser General Public License as published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version.


Contact
---

Please refer to the webpage of the [High Performance Humanoid Technologies Lab](http://h2t.anthropomatik.kit.edu/english/).
