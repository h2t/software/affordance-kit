/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Eren E. Aksoy ( eren dot aksoy at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "PrimitiveExtractor.h"

#include "primitives/Plane.h"
#include "primitives/Cylinder.h"
#include "primitives/Sphere.h"

#include <pcl/common/geometry.h>
#if PCL_VERSION > PCL_VERSION_CALC(1, 12, 0)
#include <pcl/surface/convex_hull.h>
#include <pcl/sample_consensus/sac_model_cylinder.h>
#include <pcl/sample_consensus/sac_model_sphere.h>
#endif

using namespace AffordanceKit;

PrimitiveExtractor::PrimitiveExtractor()
{
    /// Create variables needed for preparations
    inputCloudPtr.reset(new pcl::PointCloud<PointL>);
    inlierCloudPtr.reset(new pcl::PointCloud<PointL>);
    graspPoints.reset(new pcl::PointCloud<PointL>);

    verbose = false;

    planeMaxIteration = 250;
    planeDistanceThreshold = 0.03f;
    planeNormalDistanceWeight = 0.1f;
    circleDistanceThreshold = 0.01f;

    cylinderMaxIteration = 250;
    cylinderDistanceThreshold = 0.005f;
    cylinderRadiusLimit = 0.1f;

    sphereMaxIteration = 250;
    sphereDistanceThreshold = 0.9f;
    sphereNormalDistanceWeight = 0.1f;

    newLabel = 1;
}

PointLPtr& PrimitiveExtractor::computeScenePrimitives(PointLPtr& CloudPtr)
{
    // copy original points to the cloud
    pcl::copyPointCloud(*CloudPtr, *inputCloudPtr);

    // clean the previous results
    inlierCloudPtr->clear();
    graspPoints->clear();
    detectedPlaneInliers.clear();
    detectedCylinderInliers.clear();
    detectedSphereInliers.clear();
    detectedPlaneCoeff.clear();
    detectedCylinderCoeff.clear();
    detectedSphereCoeff.clear();
    detectedCircularityProbabilities.clear();
    detectedCircleCoeff.clear();
    detectedPlaneGraspPoints.clear();
    detectedCylinderGraspPoints.clear();
    detectedSphereGraspPoints.clear();

    // scan the labels
    getLabelMap();

    // compute primitives iteratively
    runIterativePrimitiveExtractor();

    // merge similar models
    //MergeSimilarPrimitives();

    computeEuclideanClusters();

    computeCirclesFromGraspPoints();

    return inlierCloudPtr;

}

PrimitiveSetPtr PrimitiveExtractor::getPrimitiveSet(int spatialStepSize, int numOrientationalSteps, long long originalTimestamp)
{
    AffordanceKit::PrimitiveSetPtr primitiveSet(new AffordanceKit::PrimitiveSet());

    for (unsigned int i = 0; i < detectedPlaneCoeff.size(); i++)
    {
        pcl::PointCloud<pcl::PointXYZL>::Ptr graspPoints = detectedPlaneGraspPoints[i];

        std::vector<Eigen::Vector3f> convexHull;
        convexHull.reserve(graspPoints->points.size());
        for (auto& p : graspPoints->points)
        {
            convexHull.push_back(Eigen::Vector3f(p.x, p.y, p.z));
        }

        AffordanceKit::PlanePtr plane(new AffordanceKit::Plane(convexHull));
        plane->sample(spatialStepSize, numOrientationalSteps);
        plane->setTimestamp(originalTimestamp);
        plane->setLabel(graspPoints->points[0].label);
        primitiveSet->push_back(plane);
    }

    for (unsigned int i = 0; i < detectedCylinderCoeff.size(); i++)
    {
        pcl::ModelCoefficients::Ptr coefficients = detectedCylinderCoeff[i];

        // Projected inliers to determine cylinder base point and length
        // (These are not properly encoded in the RANSAC coefficients)
        float min_t = std::numeric_limits<float>::infinity();
        float max_t = -std::numeric_limits<float>::infinity();

        Eigen::Vector3f b(coefficients->values[0], coefficients->values[1], coefficients->values[2]);
        Eigen::Vector3f d(coefficients->values[3], coefficients->values[4], coefficients->values[5]);
        d.normalize();

        for (auto & point : detectedCylinderInliers[i]->points)
        {
            Eigen::Vector3f v(point.x, point.y, point.z);

            float t = (v - b).dot(d) / d.dot(d);

            min_t = std::min(min_t, t);
            max_t = std::max(max_t, t);
        }

        float length = max_t - min_t;
        Eigen::Vector3f base = b + min_t * d + 0.5 * length * d;
        Eigen::Vector3f direction = d;
        float radius = coefficients->values[6];

        AffordanceKit::CylinderPtr cylinder(new AffordanceKit::Cylinder(base, direction, length, radius));
        cylinder->sample(spatialStepSize, numOrientationalSteps);
        cylinder->setTimestamp(originalTimestamp);
        cylinder->setLabel(graspPoints->points[0].label);
        primitiveSet->push_back(cylinder);
    }

    for (auto coefficients : detectedSphereCoeff)
    {
        Eigen::Vector3f center;
        center << coefficients->values[0], coefficients->values[1], coefficients->values[2];

        float radius = coefficients->values[3];

        AffordanceKit::SpherePtr sphere(new AffordanceKit::Sphere(center, radius));
        sphere->sample(spatialStepSize, numOrientationalSteps);
        sphere->setTimestamp(originalTimestamp);
        sphere->setLabel(graspPoints->points[0].label);
        primitiveSet->push_back(sphere);
    }

    return primitiveSet;
}

void PrimitiveExtractor::mergeSimilarPrimitives()
{
    // this considers only the planar surfaces...
    mergedPlaneInliers.clear();
    int memrgedIndex = -1;

    const int PlaneSize = detectedPlaneCoeff.size();
    std::vector<int> isMergedFlag(PlaneSize);

    // compute euclidean distance between planar surface models
    for (int i = 0; i < PlaneSize; i++)
    {
        pcl::ModelCoefficients::Ptr model_i = detectedPlaneCoeff[i];
        Eigen::Vector4f normal_i(model_i->values.data());   // convertion from ModelCoefficients to Eigen::Vector4f
        normal_i[3] = 0; // consider only the first three elements of the coefficients since they are the surface normal x,y,z, positions

        PointLPtr inlier_i(new pcl::PointCloud<PointL>);
        pcl::copyPointCloud(*(detectedPlaneInliers[i]), *inlier_i);

        if (isMergedFlag[i] == 0)
        {
            mergedPlaneInliers.push_back(inlier_i);
            memrgedIndex++;

            for (int j = i + 1; j < PlaneSize; j++)
            {
                if (isMergedFlag[j] == 0)
                {
                    pcl::ModelCoefficients::Ptr model_j = detectedPlaneCoeff[j];
                    Eigen::Vector4f normal_j(model_j->values.data());   // convertion from ModelCoefficients to Eigen::Vector4f
                    normal_j[3] = 0;  // consider only the first three elements of the coefficients since they are the surface normal x,y,z, positions

                    PointLPtr inlier_j(new pcl::PointCloud<PointL>);
                    pcl::copyPointCloud(*(detectedPlaneInliers[j]), *inlier_j);

                    // compute angles between surface normals
                    double angleNormal = fabs(pcl::getAngle3D(normal_i, normal_j));

                    // compute 3D spatial neighborhood relations
                    bool spatialRelation = computeNeighborhoodRelation(inlier_i, inlier_j);

                    //std::cout << " i: " << i << " j: " << j <<   " n: " << angleNormal << " r: " << spatialRelation << std::endl;

                    if (angleNormal < 0.7 && spatialRelation)  // 0.7 means less than 40 degrees
                    {
                        (*mergedPlaneInliers[memrgedIndex]) += (*inlier_j);
                        isMergedFlag[i] = 1;
                        isMergedFlag[j] = 1;
                    }
                }
            }
        }
    }
}

bool PrimitiveExtractor::computeNeighborhoodRelation(PointLPtr cloud_i, PointLPtr cloud_j)
{
    // create an kdtree
    pcl::KdTreeFLANN<pcl::PointXYZL> kdtree;
    kdtree.setInputCloud(cloud_i);

    double neighborhoodRelation = false;
    int MinDistance = 2000;

    // each point of cloud_j will be searched in the tree
    for (auto searchPoint : cloud_j->points)
    {
        int K = 10;
        std::vector<int> pointIdxNKNSearch(K);
        std::vector<float> pointNKNSquaredDistance(K);

        if (kdtree.nearestKSearch(searchPoint, K, pointIdxNKNSearch, pointNKNSquaredDistance) > 0)
        {
            if (pointNKNSquaredDistance[0] <= MinDistance)
            {
                neighborhoodRelation = true;
            }
        }

        if (neighborhoodRelation)
        {
            break;
        }
    }

    return neighborhoodRelation;
}

void PrimitiveExtractor::getLabelMap()
{
    labelMap.clear();

    std::map<uint32_t, pcl::PointIndices >::iterator iter_i;

    for (size_t i = 0; i < inputCloudPtr->points.size(); i++)
    {
        uint32_t currLabel = inputCloudPtr->points[i].label;

        if (currLabel)
        {
            iter_i = labelMap.find(currLabel);

            if (iter_i == labelMap.end())
            {
                pcl::PointIndices label_indices;
                label_indices.indices.push_back(i);
                labelMap[currLabel] = label_indices;
            }
            else
            {
                iter_i->second.indices.push_back(i);
            }
        }
    }

    if (verbose)
    {
        std::cout << " --------- Segments -----------  " << labelMap.size() <<  std::endl;

        for (iter_i = labelMap.begin(); iter_i != labelMap.end(); iter_i++)
        {
            std::cout << iter_i->first << " >> " <<  iter_i->second.indices.size() << "  " ;
        }

        std::cout << " --------------------  " <<  std::endl;
    }
}

void PrimitiveExtractor::runIterativePrimitiveExtractor()
{
    // for each big segment in the segmented cloud, run the primtive extraction
    #pragma omp parallel
    {
        unsigned int i = 0;
        unsigned int ithread = omp_get_thread_num();
        unsigned int nthreads = omp_get_num_threads();

        for (auto segment = labelMap.begin(); segment != labelMap.end(); segment++, i++)
        {
            // Fixed assignment to OpenMP threads
            if (i % nthreads != ithread)
            {
                continue;
            }

            uint32_t segmentLabel = segment->first;
            size_t segmentSize = segment->second.indices.size();

            if (segmentSize > minSegmentSize  && segmentSize < maxSegmentSize)
            {
                if (verbose)
                {
                    std::cout << " segmentLabel : " << segmentLabel << " segmentSize " << segmentSize << std::endl;
                }

                // consider only the current segment not the entire scene
                PointLPtr SegmentCloudPtr(new pcl::PointCloud<PointL>);
                pcl::copyPointCloud(*inputCloudPtr, segment->second.indices, *SegmentCloudPtr);

                // compute primitive models until there is not enough point left in the segment cloud
                while (SegmentCloudPtr->points.size() > minSegmentSize)
                {
                    pcl::PointCloud<pcl::Normal>::Ptr normals = computeNormals(SegmentCloudPtr);
                    PrimitiveModelPtr model = computeModel(SegmentCloudPtr, normals);

                    if (model->score > 0)
                    {
                        SegmentCloudPtr = computeOutliers(SegmentCloudPtr, normals, model);
                        updateInliersAndCoefficients(model);
                    }

                    if (!SegmentCloudPtr || model->score == 0)
                    {
                        break;
                    }
                }
            }
        }
    }
}

void PrimitiveExtractor::computeEuclideanClusters()
{
    inlierCloudPtr->clear();
    graspPoints->clear();

    euclideanPlaneInliers.clear();
    euclideanCylinderInliers.clear();
    euclideanSphereInliers.clear();

    euclideanPlaneGraspPoints.clear();
    euclideanCylinderGraspPoints.clear();
    euclideanSphereGraspPoints.clear();

    euclideanPlaneCoeff.clear();
    euclideanCylinderCoeff.clear();
    euclideanSphereCoeff.clear();

    std::vector<std::vector<pcl::PointIndices>> planeSubSegments(detectedPlaneInliers.size());
    std::vector<std::vector<pcl::PointIndices>> cylinderSubSegments(detectedCylinderInliers.size());
    std::vector<std::vector<pcl::PointIndices>> sphereSubSegments(detectedSphereInliers.size());

    // Compute Euclidean clusters for each found primitive
    // Parallelization is not beneficial as the underlying PCL functions already user OpenMP
    for (size_t i = 0; i < detectedPlaneInliers.size() + detectedCylinderInliers.size() + detectedSphereInliers.size(); i++)
    {
        if (i < detectedPlaneInliers.size())
        {
            computeEuclideanClusters(detectedPlaneInliers[i], planeSubSegments[i]);
        }
        else if (i < detectedPlaneInliers.size() + detectedCylinderInliers.size())
        {
            computeEuclideanClusters(detectedCylinderInliers[i - detectedPlaneInliers.size()], cylinderSubSegments[i - detectedPlaneInliers.size()]);
        }
        else
        {
            computeEuclideanClusters(detectedSphereInliers[i - detectedPlaneInliers.size() - detectedCylinderInliers.size()], sphereSubSegments[i - detectedPlaneInliers.size() - detectedCylinderInliers.size()]);
        }
    }

    // planes
    // compute euclidean distance between planar surface models
    #pragma omp parallel for
    for (size_t i = 0; i < detectedPlaneInliers.size(); i++)
    {
        pcl::ModelCoefficients::Ptr currModel = detectedPlaneCoeff[i];
        PointLPtr currInliers = detectedPlaneInliers[i];

        for (auto & subSegID : planeSubSegments[i])
        {
            PointLPtr tempCloudPtr(new pcl::PointCloud<PointL>);
            pcl::copyPointCloud(*currInliers, subSegID.indices , *tempCloudPtr);

            uint32_t label;
            #pragma omp critical
            {
                label = newLabel++;
            }

            // Before copying, assign new label to each detected primitive inliers
            for (auto & point : tempCloudPtr->points)
            {
                point.label = label;
            }

            // Compute grasp points for current cluster
            PointLPtr cloud_inliers(new pcl::PointCloud<PointL>);
            pcl::ProjectInliers<PointL> proj;
            proj.setModelType(pcl::SACMODEL_PLANE);
            proj.setInputCloud(tempCloudPtr);
            proj.setModelCoefficients(currModel);
            proj.filter(*cloud_inliers);

            PointLPtr currGraspPoints(new pcl::PointCloud<PointL>);
            pcl::ConvexHull<PointL> convexhull;
            convexhull.setInputCloud(cloud_inliers);

            #pragma omp critical
            {
                // This call seems to be not thread safe
                convexhull.reconstruct(*currGraspPoints);
            }

            // Before copying, assign new label to each detected grasp points
            for (auto & point : currGraspPoints->points)
            {
                point.label = label;
            }

            // update inliers and grasp points
            #pragma omp critical
            {
                euclideanPlaneInliers.push_back(tempCloudPtr);
                euclideanPlaneGraspPoints.push_back(currGraspPoints);
                euclideanPlaneCoeff.push_back(currModel);
                (*inlierCloudPtr) += (*tempCloudPtr);
                (*graspPoints) += (*currGraspPoints);
            }
        }
    }

    detectedPlaneCoeff.clear();
    detectedPlaneInliers.clear();
    detectedPlaneGraspPoints.clear();

    detectedPlaneCoeff = euclideanPlaneCoeff;
    detectedPlaneInliers = euclideanPlaneInliers;
    detectedPlaneGraspPoints = euclideanPlaneGraspPoints;

    // cylinders
    // compute euclidean distance between planar surface models
    #pragma omp parallel for
    for (size_t i = 0; i < detectedCylinderInliers.size(); i++)
    {
        pcl::ModelCoefficients::Ptr currModel = detectedCylinderCoeff[i];
        PointLPtr currInliers = detectedCylinderInliers[i];

        for (auto & subSegID : cylinderSubSegments[i])
        {
            PointLPtr tempCloudPtr(new pcl::PointCloud<PointL>);
            pcl::copyPointCloud(*currInliers, subSegID.indices , *tempCloudPtr);

            uint32_t label;
            #pragma omp critical
            {
                label = newLabel;
                newLabel++;
            }

            // Before copying, assign new label to each detected primitive inliers
            for (auto & point : tempCloudPtr->points)
            {
                point.label = label;
            }

            // Compute grasp points for current cluster
            PointLPtr cloud_inliers(new pcl::PointCloud<PointL>);
            pcl::ProjectInliers<PointL> proj;
            proj.setModelType(pcl::SACMODEL_PLANE);
            proj.setInputCloud(tempCloudPtr);
            proj.setModelCoefficients(currModel);
            proj.filter(*cloud_inliers);

            PointLPtr currGraspPoints(new pcl::PointCloud<PointL>);
            pcl::ConvexHull<PointL> convexhull;
            convexhull.setInputCloud(cloud_inliers);

            #pragma omp critical
            {
                // This call seems to be not thread safe
                convexhull.reconstruct(*currGraspPoints);
            }

            // Before copying, assign new label to each detected grasp points
            for (auto & point : currGraspPoints->points)
            {
                point.label = label;
            }

            // update inliers and grasp points
            #pragma omp critical
            {
                euclideanCylinderInliers.push_back(tempCloudPtr);
                euclideanCylinderGraspPoints.push_back(currGraspPoints);
                euclideanCylinderCoeff.push_back(currModel);
                (*inlierCloudPtr) += (*tempCloudPtr);
                (*graspPoints) += (*currGraspPoints);
            }
        }
    }


    detectedCylinderCoeff.clear();
    detectedCylinderInliers.clear();
    detectedCylinderGraspPoints.clear();

    detectedCylinderCoeff = euclideanCylinderCoeff;
    detectedCylinderInliers = euclideanCylinderInliers;
    detectedCylinderGraspPoints = euclideanCylinderGraspPoints;


    // sphere
    // compute euclidean distance between planar surface models
    #pragma omp parallel for
    for (size_t i = 0; i < detectedSphereInliers.size(); i++)
    {
        pcl::ModelCoefficients::Ptr currModel = detectedSphereCoeff[i];
        PointLPtr currInliers = detectedSphereInliers[i];

        for (auto & subSegID : sphereSubSegments[i])
        {
            PointLPtr tempCloudPtr(new pcl::PointCloud<PointL>);
            pcl::copyPointCloud(*currInliers, subSegID.indices , *tempCloudPtr);

            uint32_t label;
            #pragma omp critical
            {
                label = newLabel;
                newLabel++;
            }

            // Before copying, assign new label to each detected primitive inliers
            for (auto & point : tempCloudPtr->points)
            {
                point.label = label;
            }

            // Compute grasp points for current cluster
            PointLPtr cloud_inliers(new pcl::PointCloud<PointL>);
            pcl::ProjectInliers<PointL> proj;
            proj.setModelType(pcl::SACMODEL_PLANE);
            proj.setInputCloud(tempCloudPtr);
            proj.setModelCoefficients(currModel);
            proj.filter(*cloud_inliers);

            PointLPtr currGraspPoints(new pcl::PointCloud<PointL>);
            pcl::ConvexHull<PointL> convexhull;
            convexhull.setInputCloud(cloud_inliers);

            #pragma omp critical
            {
                // This call seems to be not thread safe
                convexhull.reconstruct(*currGraspPoints);
            }

            // Before copying, assign new label to each detected grasp points
            for (auto & point : currGraspPoints->points)
            {
                point.label = label;
            }

            // update inliers and grasp points
            #pragma omp critical
            {
                euclideanSphereInliers.push_back(tempCloudPtr);
                euclideanSphereGraspPoints.push_back(currGraspPoints);
                euclideanSphereCoeff.push_back(currModel);
                (*inlierCloudPtr) += (*tempCloudPtr);
                (*graspPoints) += (*currGraspPoints);
            }
        }
    }

    detectedSphereCoeff.clear();
    detectedSphereInliers.clear();
    detectedSphereGraspPoints.clear();

    detectedSphereCoeff = euclideanSphereCoeff;
    detectedSphereInliers = euclideanSphereInliers;
    detectedSphereGraspPoints = euclideanSphereGraspPoints;

}

void PrimitiveExtractor::computeEuclideanClusters(const PointLPtr& inliers, std::vector<pcl::PointIndices>& segments)
{
    // Creating the KdTree object for the search method of the extraction
    pcl::search::KdTree<pcl::PointXYZL>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZL>(false));
    tree->setInputCloud(inliers);

    // Clustering based on euclidean distances
    pcl::EuclideanClusterExtraction<pcl::PointXYZL> ec;
    ec.setClusterTolerance(euclideanClusteringTolerance);
    ec.setInputCloud(inliers);
    ec.setSearchMethod(tree);
    ec.setMinClusterSize(minSegmentSize);
    ec.setMaxClusterSize(maxSegmentSize);
    ec.extract(segments);
}

void PrimitiveExtractor::computeCirclesFromGraspPoints()
{
    // we compute cirlces from grasp points of planar surfaces...e.g. needed for valve detection
    for (auto & detectedPlaneGraspPoint : detectedPlaneGraspPoints)
    {
        //std::cout<< "current plane grasp point size: " <<  detectedPlaneGraspPoints[i]->points.size()  << std::endl;
        double circleMaxIteration = planeMaxIteration;

        pcl::ModelCoefficients::Ptr circleCoefficients(new pcl::ModelCoefficients);
        pcl::PointIndices::Ptr circleInliers(new pcl::PointIndices);

        // Create the segmentation object
        pcl::SACSegmentation<pcl::PointXYZL> segCircle;
        segCircle.setOptimizeCoefficients(true);
        segCircle.setModelType(pcl::SACMODEL_CIRCLE3D);
        segCircle.setMethodType(pcl::SAC_RANSAC);
        segCircle.setDistanceThreshold(circleDistanceThreshold);
        segCircle.setMaxIterations(circleMaxIteration);
        segCircle.setInputCloud(detectedPlaneGraspPoint);
        segCircle.segment(*circleInliers, *circleCoefficients);

        float currProbability = 1.0 * circleInliers->indices.size() / detectedPlaneGraspPoint->points.size();
        detectedCircularityProbabilities.push_back(currProbability);
        detectedCircleCoeff.push_back(circleCoefficients);

        if (verbose && circleCoefficients->values.size())
        {
            std::cout << " ============= currProbability ============ " << currProbability << std::endl;
            std::cout << " ============= Circle Found ============ " << std::endl;
            std::cout << " Position = x: " << circleCoefficients->values[0] << " y: " << circleCoefficients->values[1] << " z: " << circleCoefficients->values[2] << " radius: " << circleCoefficients->values[3] << std::endl;
            std::cout << " cloud size " << detectedPlaneGraspPoint->points.size() << "  circleInliers " << circleInliers->indices.size() << std::endl;
            std::cout << " ======================================= " << std::endl;
        }
    }
}

PrimitiveModelPtr PrimitiveExtractor::computeModel(const PointLPtr& segmentPointCloud, const pcl::PointCloud<pcl::Normal>::Ptr& normals)
{
    if (normals->points.size() > minSegmentSize)
    {
        PrimitiveModelPtr planeModel(nullptr);
        PrimitiveModelPtr cylinderModel(nullptr);
        PrimitiveModelPtr sphereModel(nullptr);

        #pragma omp parallel sections
        {
            #pragma omp section
            {
                planeModel = calculatePlaneCoefficients(segmentPointCloud, normals);
            }

            #pragma omp section
            {
                cylinderModel = calculateCylinderCoefficients(segmentPointCloud, normals);
            }

            // Spheres are currently not supported
            /*#pragma omp section
            {
                sphereModel = CalculateSphereCoefficients();
            }*/

            //CalculateLineCoefficients();
        }

        if (planeModel && planeModel->score > cylinderModel->score)
        {
            return planeModel;
        }
        else if (cylinderModel)
        {
            return cylinderModel;
        }
    }

    return nullptr;
}

pcl::PointCloud<pcl::Normal>::Ptr PrimitiveExtractor::computeNormals(const PointLPtr& segmentPointCloud)
{
    pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>);

    // Creating the KdTree object for the search method of the extraction
    pcl::search::KdTree<PointL>::Ptr tree(new pcl::search::KdTree<PointL>());
    tree->setInputCloud(segmentPointCloud);

    pcl::NormalEstimation<PointL, pcl::Normal> normalEstimator;
    normalEstimator.setSearchMethod(tree);
    normalEstimator.setInputCloud(segmentPointCloud);
    normalEstimator.setKSearch(50);
    normalEstimator.setViewPoint(std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max());
    normalEstimator.compute(*normals);

    if (verbose)
    {
        std::cout << " size of normals: " << normals->points.size() << std::endl;
    }

    return normals;
}

void PrimitiveExtractor::updateInliersAndCoefficients(PrimitiveModelPtr& model)
{
    uint32_t label;
    #pragma omp critical
    {
        label = newLabel;
        newLabel++;
    }

    // Before copying, assign new label to each detected primitive inliers
    for (auto & point : model->inliers->points)
    {
        point.label = label;
    }

    // Before copying, assign new label to each detected primitive inliers
    for (auto & point : model->graspPoints->points)
    {
        point.label = label;
    }

    // after computing the outliers we need to update the inliers
    // pushing back the detected model inliers and coefficients
    #pragma omp critical
    {
        if (model->modelType == "plane")
        {
            detectedPlaneInliers.push_back(model->inliers);
            detectedPlaneGraspPoints.push_back(model->graspPoints);
            detectedPlaneCoeff.push_back(model->modelCoefficients);
        }

        if (model->modelType == "cylinder")
        {
            detectedCylinderInliers.push_back(model->inliers);
            detectedCylinderGraspPoints.push_back(model->graspPoints);
            detectedCylinderCoeff.push_back(model->modelCoefficients);
        }

        if (model->modelType == "sphere")
        {
            detectedSphereInliers.push_back(model->inliers);
            detectedSphereGraspPoints.push_back(model->graspPoints);
            detectedSphereCoeff.push_back(model->modelCoefficients);
        }
    }

    if (verbose)
    {
        std::cout << " ============== final ====================== " << std::endl;

        if (model->score > 0)
        {
            std::cout << " detectedScore: " << model->score << " detectedModelType " << model->modelType << std::endl;
            std::cout << " detectedModelcoeffs: " << model->modelCoefficients->values[0] << " " << model->modelCoefficients->values[1] << " " << model->modelCoefficients->values[2] << std::endl;
        }
        else
        {
            std::cout << " detectedScore: " << model->score << " No Model Found" << std::endl;
        }
    }
}

PointLPtr PrimitiveExtractor::computeOutliers(const PointLPtr& segmentPointCloud, const pcl::PointCloud<pcl::Normal>::Ptr& normals, PrimitiveModelPtr& model)
{
    if (model->modelType == "No Model")
    {
        return nullptr;
    }

    PointLPtr detectedOutliers(new pcl::PointCloud<PointL>);

    if (model->modelType == "plane")
    {
        //std::cout<< " ********* Extracting Plane Outliers for the Next Iteration" << std::endl;
        // get inliers within a certain distance
        pcl::SampleConsensusModelPlane<PointL> model_plane(segmentPointCloud);
        model_plane.setInputCloud(segmentPointCloud);
        Eigen::VectorXf coeff = Eigen::VectorXf::Map(&model->modelCoefficients->values[0], model->modelCoefficients->values.size());
        std::vector<int> inliersWithinDistance;
        model_plane.selectWithinDistance(coeff, outlierThreshold, inliersWithinDistance);

        // Extract outliers which are the all points except the above detected inliers within a certain distance
        pcl::ExtractIndices<PointL> extract;
        extract.setInputCloud(segmentPointCloud);
        pcl::PointIndices::Ptr convertedInliers(new pcl::PointIndices);
        convertedInliers->indices = inliersWithinDistance; // convert from std::vector<int> to pcl::PointIndices
        extract.setIndices(convertedInliers);
        extract.setNegative(true);  // to select outliers
        extract.filter(*detectedOutliers);

        pcl::copyPointCloud(*segmentPointCloud, convertedInliers->indices , *model->inliers);  // update the inliers
    }

    if (model->modelType == "cylinder")
    {
        //std::cout<< " ********* Extracting Cylinder Outliers for the Next Iteration" << std::endl;
        // get inliers within a certain distance
        pcl::SampleConsensusModelCylinder<PointL, pcl::Normal> model_cylinder(segmentPointCloud);
        model_cylinder.setInputCloud(segmentPointCloud);
        model_cylinder.setInputNormals(normals);
        Eigen::VectorXf coeff = Eigen::VectorXf::Map(&model->modelCoefficients->values[0], model->modelCoefficients->values.size());
        std::vector<int> inliersWithinDistance;
        model_cylinder.selectWithinDistance(coeff, outlierThreshold, inliersWithinDistance);

        // Extract outliers which are the all points except the above detected inliers within a certain distance
        pcl::ExtractIndices<PointL> extract;
        extract.setInputCloud(segmentPointCloud);
        pcl::PointIndices::Ptr convertedInliers(new pcl::PointIndices);
        convertedInliers->indices = inliersWithinDistance; // convert from std::vector<int> to pcl::PointIndices
        extract.setIndices(convertedInliers);
        extract.setNegative(true);  // to select outliers
        extract.filter(*detectedOutliers);

        pcl::copyPointCloud(*segmentPointCloud, convertedInliers->indices , *model->inliers);  // update the inliers
    }

    if (model->modelType == "sphere")
    {
        //std::cout<< " ********* Extracting Sphere Outliers for the Next Iteration" << std::endl;
        // get inliers within a certain distance
        pcl::SampleConsensusModelSphere<PointL> model_sphere(segmentPointCloud);
        model_sphere.setInputCloud(segmentPointCloud);
        Eigen::VectorXf coeff = Eigen::VectorXf::Map(&model->modelCoefficients->values[0], model->modelCoefficients->values.size());
        std::vector<int> inliersWithinDistance;
        model_sphere.selectWithinDistance(coeff, outlierThreshold, inliersWithinDistance);

        // Extract outliers which are the all points except the above detected inliers within a certain distance
        pcl::ExtractIndices<PointL> extract;
        extract.setInputCloud(segmentPointCloud);
        pcl::PointIndices::Ptr convertedInliers(new pcl::PointIndices);
        convertedInliers->indices = inliersWithinDistance; // convert from std::vector<int> to pcl::PointIndices
        extract.setIndices(convertedInliers);
        extract.setNegative(true);  // to select outliers
        extract.filter(*detectedOutliers);

        pcl::copyPointCloud(*segmentPointCloud, convertedInliers->indices , *model->inliers);  // update the inliers
    }

    if (verbose)
    {
        std::cout << " ---------- ComputeOutliers ---------- " << segmentPointCloud->points.size() << " --- " << model->inliers->points.size() << " --- " << detectedOutliers->points.size()  << std::endl;
    }

    return detectedOutliers;
}

PrimitiveModelPtr PrimitiveExtractor::calculatePlaneCoefficients(const PointLPtr& segmentPointCloud, const pcl::PointCloud<pcl::Normal>::Ptr& normals)
{
    PrimitiveModelPtr plane(new PrimitiveModel);
    plane->modelType = "plane";

    // try plane  segmentation
    pcl::PointIndices::Ptr plane_inliers(new pcl::PointIndices);
    pcl::ModelCoefficients::Ptr plane_coefficients(new pcl::ModelCoefficients);

    pcl::SACSegmentationFromNormals<PointL, pcl::Normal> seg;
    seg.setOptimizeCoefficients(true);
    seg.setModelType(pcl::SACMODEL_NORMAL_PLANE);
    seg.setNormalDistanceWeight(planeNormalDistanceWeight);
    seg.setMethodType(pcl::SAC_RANSAC);
    seg.setProbability(0.99);
    seg.setMaxIterations(planeMaxIteration);
    seg.setDistanceThreshold(planeDistanceThreshold);
    seg.setInputCloud(segmentPointCloud);
    seg.setInputNormals(normals);
    seg.segment(*plane_inliers, *plane_coefficients);

    plane->score = plane_inliers->indices.size();

    if (plane->score > 0)
    {
        // Project the model inliers
        plane->inliers.reset(new pcl::PointCloud<PointL>);
        pcl::ProjectInliers<PointL> proj;
        proj.setModelType(pcl::SACMODEL_PLANE);
        proj.setIndices(plane_inliers);
        proj.setInputCloud(segmentPointCloud);
        proj.setModelCoefficients(plane_coefficients);
        proj.filter(*plane->inliers);

        // Create a convexhull around the projected inliers that represents potential grasp points
        plane->graspPoints.reset(new pcl::PointCloud<PointL>);
        pcl::ConvexHull<PointL> convexhull;
        convexhull.setInputCloud(plane->inliers);

        #pragma omp critical
        {
            // Not thread safe
            convexhull.reconstruct(*plane->graspPoints);
        }

        plane->modelCoefficients.reset(new pcl::ModelCoefficients);
        plane->modelCoefficients->values.insert(plane->modelCoefficients->values.begin(), plane_coefficients->values.begin(), plane_coefficients->values.end());
    }

    if (verbose)
    {
        std::cout << " ===================================== " << std::endl;
        std::cout << " POSE score for PLANE " << plane->score << std::endl;
        std::cout << " Position = x: " << plane_coefficients->values[0] << " y: " << plane_coefficients->values[1] << " z: " << plane_coefficients->values[2] << " h: " << plane_coefficients->values[3] << std::endl;
        std::cout << " cloud size " << segmentPointCloud->points.size() << "  inliers " << plane_inliers->indices.size() << std::endl;
        std::cout << " ===================================== " << std::endl;
    }

    return plane;
}

PrimitiveModelPtr PrimitiveExtractor::calculateCylinderCoefficients(const PointLPtr& segmentPointCloud, const pcl::PointCloud<pcl::Normal>::Ptr& normals)
{
    PrimitiveModelPtr cylinder(new PrimitiveModel);
    cylinder->modelType = "cylinder";

    pcl::PointIndices::Ptr cylinder_inliers(new pcl::PointIndices);
    pcl::ModelCoefficients::Ptr cylinder_coefficients(new pcl::ModelCoefficients);

    pcl::SACSegmentationFromNormals<PointL, pcl::Normal> seg;
    seg.setOptimizeCoefficients(true);
    seg.setModelType(pcl::SACMODEL_CYLINDER);
    seg.setMethodType(pcl::SAC_RANSAC);
    seg.setNormalDistanceWeight(0.1);
    seg.setMaxIterations(cylinderMaxIteration);
    seg.setDistanceThreshold(cylinderDistanceThreshold);
    seg.setRadiusLimits(0, cylinderRadiusLimit);
    seg.setInputCloud(segmentPointCloud);
    seg.setInputNormals(normals);
    seg.segment(*cylinder_inliers, *cylinder_coefficients);

    cylinder->score = cylinder_inliers->indices.size();

    if (cylinder->score > 0)
    {
        // Project the model inliers
        cylinder->inliers.reset(new pcl::PointCloud<PointL>);
        pcl::ProjectInliers<PointL> proj;
        proj.setModelType(pcl::SACMODEL_CYLINDER);
        proj.setIndices(cylinder_inliers);
        proj.setInputCloud(segmentPointCloud);
        proj.setModelCoefficients(cylinder_coefficients);
        proj.filter(*cylinder->inliers);

        // Create a convexhull around the projected inliers that represents potential grasp points
        cylinder->graspPoints.reset(new pcl::PointCloud<PointL>);
        pcl::ConvexHull<PointL> convexhull;
        convexhull.setInputCloud(cylinder->inliers);

        #pragma omp critical
        {
            // Not thread safe
            convexhull.reconstruct(*cylinder->graspPoints);
        }

        cylinder->modelCoefficients.reset(new pcl::ModelCoefficients);
        cylinder->modelCoefficients->values.insert(cylinder->modelCoefficients->values.begin(), cylinder_coefficients->values.begin(), cylinder_coefficients->values.end());
    }

    if (verbose)
    {
        std::cout << " ===================================== " << std::endl;
        std::cout << " POSE score for CYLINDER " << cylinder->score << std::endl;
        std::cout << " Position = x: " << cylinder_coefficients->values[0] << " y: " << cylinder_coefficients->values[1] << " z: " << cylinder_coefficients->values[2]  << std::endl;
        std::cout << " Oriantation = x: " << cylinder_coefficients->values[3] << " y: " << cylinder_coefficients->values[4] << " z: " << cylinder_coefficients->values[5]  << std::endl;
        std::cout << " Radious : " << cylinder_coefficients->values[6]  << std::endl;
        std::cout << " cloud size " << segmentPointCloud->points.size() << "  inliers " << cylinder_inliers->indices.size() << std::endl;
        std::cout << " ===================================== " << std::endl;
    }

    return cylinder;
}

PrimitiveModelPtr PrimitiveExtractor::calculateSphereCoefficients(const PointLPtr& segmentPointCloud, const pcl::PointCloud<pcl::Normal>::Ptr& normals)
{
    PrimitiveModelPtr sphere(new PrimitiveModel);
    sphere->modelType = "sphere";

    pcl::PointIndices::Ptr sphere_inliers(new pcl::PointIndices);
    pcl::ModelCoefficients::Ptr sphere_coefficients(new pcl::ModelCoefficients);

    pcl::SACSegmentationFromNormals<PointL, pcl::Normal> seg;
    seg.setOptimizeCoefficients(true);
    seg.setModelType(pcl::SACMODEL_SPHERE);
    seg.setNormalDistanceWeight(sphereNormalDistanceWeight);
    seg.setMethodType(pcl::SAC_RANSAC);
    seg.setMaxIterations(sphereMaxIteration);
    seg.setDistanceThreshold(sphereDistanceThreshold);
    seg.setInputCloud(segmentPointCloud);
    seg.setInputNormals(normals);
    seg.segment(*sphere_inliers, *sphere_coefficients);

    sphere->score = sphere_inliers->indices.size();

    if (sphere->score > 0)
    {
        // Project the model inliers
        sphere->inliers.reset(new pcl::PointCloud<PointL>);
        pcl::ProjectInliers<PointL> proj;
        proj.setModelType(pcl::SACMODEL_SPHERE);
        proj.setIndices(sphere_inliers);
        proj.setInputCloud(segmentPointCloud);
        proj.setModelCoefficients(sphere_coefficients);
        proj.filter(*sphere->inliers);

        // Create a convexhull around the projected inliers that represents potential grasp points
        sphere->graspPoints.reset(new pcl::PointCloud<PointL>);
        pcl::ConvexHull<PointL> convexhull;
        convexhull.setInputCloud(sphere->inliers);

        #pragma omp critical
        {
            // Not thread safe
            convexhull.reconstruct(*sphere->graspPoints);
        }

        sphere->modelCoefficients.reset(new pcl::ModelCoefficients);
        sphere->modelCoefficients->values.insert(sphere->modelCoefficients->values.begin(), sphere_coefficients->values.begin(), sphere_coefficients->values.end());
    }

    if (verbose)
    {
        std::cout << " ===================================== " << std::endl;
        std::cout << " POSE score for SPHERE " << sphere->score << std::endl;
        std::cout << " Position = x: " << sphere_coefficients->values[0] << " y: " << sphere_coefficients->values[1] << " z: " << sphere_coefficients->values[2] << " r: " << sphere_coefficients->values[3] <<  std::endl;
        std::cout << " cloud size " << segmentPointCloud->points.size() << "  inliers " << sphere_inliers->indices.size() << std::endl;
        std::cout << " ===================================== " << std::endl;
    }

    return sphere;
}

void PrimitiveExtractor::updateParameters(bool verbose, size_t minSegmentSize, size_t maxSegmentSize, double pMaxIter, float pDistThres, float pNorDist, double cMaxIter, float cDistThres, float cRadLim, double sMaxIter, float sDistThres, float sNorDist, float  euclClusTolerance, float outlThres, float circleDistThres)
{
    this->verbose = verbose;
    this->minSegmentSize = minSegmentSize;
    this->maxSegmentSize = maxSegmentSize;
    planeMaxIteration = pMaxIter;
    planeDistanceThreshold = pDistThres;
    planeNormalDistanceWeight = pNorDist;

    circleDistanceThreshold = circleDistThres;

    cylinderMaxIteration = cMaxIter;
    cylinderDistanceThreshold = cDistThres;
    cylinderRadiusLimit = cRadLim;

    sphereMaxIteration = sMaxIter;
    sphereDistanceThreshold = sDistThres;
    sphereNormalDistanceWeight = sNorDist;

    euclideanClusteringTolerance = euclClusTolerance;
    outlierThreshold = outlThres;

    if (verbose)
    {
        std::cout << "=Primitive Extractor Parameters updated! >>  MinSegmentSize : " << minSegmentSize << " MaxSegmentSize " << maxSegmentSize << " planeMaxIteration : " << planeMaxIteration   << "  planeDistanceThreshold: " << planeDistanceThreshold
                  << " planeNormalDistanceWeight: " << planeNormalDistanceWeight << " cylinderMaxIteration: " << cylinderMaxIteration << " cylinderDistanceThreshold: " << cylinderDistanceThreshold
                  << " cylinderRadiusLimit: " << cylinderRadiusLimit    << " sphereMaxIteration: " << sphereMaxIteration << " sphereDistanceThreshold: " << sphereDistanceThreshold
                  << " sphereNormalDistanceWeight: " << sphereNormalDistanceWeight <<   " euclideanClusteringTolerance: " << euclideanClusteringTolerance << " outlierThreshol: " << outlierThreshold << " circleDistThres " << circleDistThres << std::endl;
    }
}
