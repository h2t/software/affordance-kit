/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _AffordanceKit_Affordance_H
#define _AffordanceKit_Affordance_H

#include "PrimitiveSet.h"
#include "Embodiment.h"
#include "Util.h"
#include "Belief.h"
#include "Observation.h"
#include "ThetaFunction.h"

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/serialization/shared_ptr.hpp>

namespace AffordanceKit
{
    class Affordance
    {
        friend class AffordanceSet;

    protected:
        Affordance();

    public:
        Affordance(const EmbodimentPtr& embodiment, const std::string& name, float affordanceExtractionCertainty = 1.0f);

        virtual void evaluatePrimitiveSet(const PrimitiveSetPtr& primitiveSet) = 0;
        virtual void evaluatePrimitive(const PrimitivePtr& primitive) = 0;

        void reset();

        const std::string& getName() const;

        void setTimestamp(long timestamp);
        long getTimestamp() const;

        void setTheta(const ThetaFunctionPtr& theta);
        const ThetaFunctionPtr& getTheta();
        Belief getTheta(const PrimitivePair& primitives, unsigned int index);
        Belief getTheta(const PrimitivePair& primitives, unsigned int index1, unsigned int index2);
        const Eigen::MatrixXf& getTheta(const PrimitivePair& primitives);

        unsigned int getThetaSize(const PrimitivePtr& primitive) const;
        unsigned int getThetaSize(const PrimitivePair& primitives) const;
        unsigned int getThetaSize() const;

        unsigned int getNumPrimitives() const;

        bool existsForPrimitive(const PrimitivePtr& primitive);
        void removeForPrimitive(const PrimitivePtr& primitive);

        void addObservation(const ObservationPtr& observation);
        const std::vector<ObservationPtr>& getObservations();

        float getAffordanceExtractionCertainty() const;

    protected:
        bool isValidThetaFunction(const ThetaFunctionPtr& theta);

        virtual void preEvaluatePrimitive(const PrimitivePtr& primitive) = 0;

    protected:
        long timestamp;

        EmbodimentPtr embodiment;

        std::string name;

        ThetaFunctionPtr theta;

        std::vector<ObservationPtr> observations;

        float affordanceExtractionCertainty;

    private:
        friend class boost::serialization::access;

        template<class Archive> void save(Archive& ar, const unsigned int version) const
        {
            ar& name& timestamp& affordanceExtractionCertainty& embodiment& observations;

            unsigned int sz = theta->size();
            ar& sz;
            for (auto& entry : *theta)
            {
                unsigned int rows = entry.second.rows();
                unsigned int cols = entry.second.cols();

                ar& entry.first.first& entry.first.second& rows& cols;

                for (unsigned int i = 0; i < entry.second.rows(); i++)
                {
                    for (unsigned int j = 0; j < entry.second.cols(); j++)
                    {
                        ar& entry.second(i, j);
                    }
                }
            }
        }

        template<class Archive> void load(Archive& ar, const unsigned int version)
        {
            ar& name& timestamp& affordanceExtractionCertainty& embodiment& observations;

            theta.reset(new ThetaFunction());

            unsigned int sz;
            ar& sz;
            for (unsigned int i = 0; i < sz; i++)
            {
                PrimitivePtr p1, p2;
                unsigned int rows, cols;

                ar& p1& p2& rows& cols;

                Eigen::MatrixXf M(rows, cols);
                for (unsigned int j = 0; j < rows; j++)
                {
                    for (unsigned int k = 0; k < cols; k++)
                    {
                        ar& M(j, k);
                    }
                }

                (*theta)[PrimitivePair(p1, p2)] = M;
            }
        }

        BOOST_SERIALIZATION_SPLIT_MEMBER()
    };

    typedef std::shared_ptr<Affordance> AffordancePtr;
}

#endif
