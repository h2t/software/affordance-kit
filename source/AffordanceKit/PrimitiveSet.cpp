/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "PrimitiveSet.h"

using namespace AffordanceKit;

PrimitiveSet::PrimitiveSet()
= default;

void PrimitiveSet::sample(float spatialStepSize, int numOrientationalSteps, unsigned int maxPrimitiveSamplingSize)
{
    for (auto& primitive : *this)
    {
        primitive->sample(spatialStepSize, numOrientationalSteps, maxPrimitiveSamplingSize);
    }
}

PrimitivePtr PrimitiveSet::getPrimitiveById(const std::string& id)
{
    for (auto& primitive : *this)
    {
        if (primitive->getId() == id)
        {
            return primitive;
        }
    }

    return nullptr;
}

void PrimitiveSet::getPrimitiveIds(std::vector<std::string>& ids)
{
    ids.clear();
    for (auto& primitive : *this)
    {
        ids.push_back(primitive->getId());
    }
}

float AffordanceKit::PrimitiveSet::getTotalArea() const
{
    float area = 0;

    for (auto& primitive : *this)
    {
        area += primitive->getArea();
    }

    return area;
}

float PrimitiveSet::getTotalSamplingSize() const
{
    float size = 0;

    for (auto& primitive : *this)
    {
        size += primitive->getSamplingSize();
    }

    return size;
}
