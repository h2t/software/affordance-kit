/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _AffordanceKit_TurnAffordance_H
#define _AffordanceKit_TurnAffordance_H

#include "../UnimanualAffordance.h"
#include "../PropertyBelief.h"
#include "PrismaticGraspAffordance.h"

namespace AffordanceKit
{

    class TurnAffordance : public UnimanualAffordance
    {
    public:
        TurnAffordance(const EmbodimentPtr& embodiment = EmbodimentPtr(new Embodiment()),
                       const PrismaticGraspAffordancePtr& prismaticGraspAffordance = PrismaticGraspAffordancePtr(new PrismaticGraspAffordance())) :
            UnimanualAffordance(embodiment, "Tn"),
            prismaticGraspAffordance(prismaticGraspAffordance)
        {
        }

    protected:
        Belief evaluateTheta(const PrimitivePtr& primitive, unsigned int index) const override
        {
            // The sample should allow a prismatic grasp
            Belief lowerLevelAffordance = prismaticGraspAffordance->getTheta(primitive, index);

            // The primitive shape should be approximately circular
            Belief circular = PropertyBelief::Circular(primitive);

            Belief b_movable = PropertyBelief::Movable(primitive, embodiment);

            return lowerLevelAffordance.logicAnd(circular).logicAnd(b_movable);
        }

        void preEvaluatePrimitive(const PrimitivePtr& primitive) override
        {
            prismaticGraspAffordance->evaluatePrimitive(primitive);
        }

    protected:
        PrismaticGraspAffordancePtr prismaticGraspAffordance;

    private:
        friend class boost::serialization::access;

        template<class Archive> void serialize(Archive& ar, const unsigned int version)
        {
            ar& boost::serialization::base_object<UnimanualAffordance>(*this);
            ar& prismaticGraspAffordance;
        }
    };

    typedef std::shared_ptr<TurnAffordance> TurnAffordancePtr;
}

BOOST_CLASS_EXPORT_KEY(AffordanceKit::TurnAffordance)
BOOST_CLASS_EXPORT_IMPLEMENT(AffordanceKit::TurnAffordance)

#endif




