/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _AffordanceKit_BimanualOpposedPrismaticGraspAffordance_H
#define _AffordanceKit_BimanualOpposedPrismaticGraspAffordance_H

#include "../BimanualAffordance.h"
#include "../PropertyBelief.h"
#include "BimanualPrismaticGraspAffordance.h"
#include "BimanualOpposedGraspAffordance.h"

namespace AffordanceKit
{

    class BimanualOpposedPrismaticGraspAffordance : public BimanualAffordance
    {
    public:
        BimanualOpposedPrismaticGraspAffordance(const EmbodimentPtr& embodiment = EmbodimentPtr(new Embodiment()),
                                                const BimanualPrismaticGraspAffordancePtr& bimanualPrismaticGraspAffordance = BimanualPrismaticGraspAffordancePtr(new BimanualPrismaticGraspAffordance()),
                                                const BimanualOpposedGraspAffordancePtr& bimanualOpposedGraspAffordance = BimanualOpposedGraspAffordancePtr(new BimanualOpposedGraspAffordance())) :
            BimanualAffordance(embodiment, "Bi-Op-G-Pr"),
            bimanualPrismaticGraspAffordance(bimanualPrismaticGraspAffordance),
            bimanualOpposedGraspAffordance(bimanualOpposedGraspAffordance)
        {
        }

    protected:
        Belief evaluateTheta(const PrimitivePair& primitives, unsigned int samplingIndex1, unsigned int samplingIndex2) const override
        {
            Belief lowerLevelAffordance1 = bimanualPrismaticGraspAffordance->getTheta(primitives, samplingIndex1, samplingIndex2);
            Belief lowerLevelAffordance2 = bimanualOpposedGraspAffordance->getTheta(primitives, samplingIndex1, samplingIndex2);

            return lowerLevelAffordance1.logicAnd(lowerLevelAffordance2);
        }

        void preEvaluatePrimitive(const PrimitivePtr& primitive) override
        {
            bimanualPrismaticGraspAffordance->evaluatePrimitive(primitive);
            bimanualOpposedGraspAffordance->evaluatePrimitive(primitive);
        }

    protected:
        BimanualPrismaticGraspAffordancePtr bimanualPrismaticGraspAffordance;
        BimanualOpposedGraspAffordancePtr bimanualOpposedGraspAffordance;

    private:
        friend class boost::serialization::access;

        template<class Archive> void serialize(Archive& ar, const unsigned int version)
        {
            ar& boost::serialization::base_object<BimanualAffordance>(*this);
            ar& bimanualPrismaticGraspAffordance& bimanualOpposedGraspAffordance;
        }
    };

    typedef std::shared_ptr<BimanualOpposedPrismaticGraspAffordance> BimanualOpposedPrismaticGraspAffordancePtr;
}

BOOST_CLASS_EXPORT_KEY(AffordanceKit::BimanualOpposedPrismaticGraspAffordance)
BOOST_CLASS_EXPORT_IMPLEMENT(AffordanceKit::BimanualOpposedPrismaticGraspAffordance)

#endif




