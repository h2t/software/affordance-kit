/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _AffordanceKit_BimanualAlignedPlatformGraspAffordance_H
#define _AffordanceKit_BimanualAlignedPlatformGraspAffordance_H

#include "../BimanualAffordance.h"
#include "../PropertyBelief.h"
#include "BimanualPlatformGraspAffordance.h"
#include "BimanualAlignedGraspAffordance.h"

#include <memory>

namespace AffordanceKit
{

    class BimanualAlignedPlatformGraspAffordance : public BimanualAffordance
    {
    public:
        BimanualAlignedPlatformGraspAffordance(const EmbodimentPtr& embodiment = EmbodimentPtr(new Embodiment()),
                                               const BimanualPlatformGraspAffordancePtr& bimanualPlatformGraspAffordance = BimanualPlatformGraspAffordancePtr(new BimanualPlatformGraspAffordance()),
                                               const BimanualAlignedGraspAffordancePtr& bimanualAlignedGraspAffordance = BimanualAlignedGraspAffordancePtr(new BimanualAlignedGraspAffordance())) :
            BimanualAffordance(embodiment, "Bi-Al-G-Pl"),
            bimanualPlatformGraspAffordance(bimanualPlatformGraspAffordance),
            bimanualAlignedGraspAffordance(bimanualAlignedGraspAffordance)
        {
        }

    protected:
        Belief evaluateTheta(const PrimitivePair& primitives, unsigned int samplingIndex1, unsigned int samplingIndex2) const override
        {
            Belief lowerLevelAffordance1 = bimanualPlatformGraspAffordance->getTheta(primitives, samplingIndex1, samplingIndex2);
            Belief lowerLevelAffordance2 = bimanualAlignedGraspAffordance->getTheta(primitives, samplingIndex1, samplingIndex2);

            return lowerLevelAffordance1.logicAnd(lowerLevelAffordance2);
        }

        void preEvaluatePrimitive(const PrimitivePtr& primitive) override
        {
            bimanualPlatformGraspAffordance->evaluatePrimitive(primitive);
            bimanualAlignedGraspAffordance->evaluatePrimitive(primitive);
        }

    protected:
        BimanualPlatformGraspAffordancePtr bimanualPlatformGraspAffordance;
        BimanualAlignedGraspAffordancePtr bimanualAlignedGraspAffordance;

    private:
        friend class boost::serialization::access;

        template<class Archive> void serialize(Archive& ar, const unsigned int version)
        {
            ar& boost::serialization::base_object<BimanualAffordance>(*this);
            ar& bimanualPlatformGraspAffordance& bimanualAlignedGraspAffordance;
        }
    };

    typedef std::shared_ptr<BimanualAlignedPlatformGraspAffordance> BimanualAlignedPlatformGraspAffordancePtr;
}

BOOST_CLASS_EXPORT_KEY(AffordanceKit::BimanualAlignedPlatformGraspAffordance)
BOOST_CLASS_EXPORT_IMPLEMENT(AffordanceKit::BimanualAlignedPlatformGraspAffordance)

#endif



