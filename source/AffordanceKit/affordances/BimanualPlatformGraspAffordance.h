/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _AffordanceKit_BimanualPlatformGraspAffordance_H
#define _AffordanceKit_BimanualPlatformGraspAffordance_H

#include "../BimanualAffordance.h"
#include "../PropertyBelief.h"
#include "PlatformGraspAffordance.h"

namespace AffordanceKit
{
    class BimanualPlatformGraspAffordance : public BimanualAffordance
    {
    public:
        BimanualPlatformGraspAffordance(const EmbodimentPtr& embodiment = EmbodimentPtr(new Embodiment()),
                                        const PlatformGraspAffordancePtr& platformGraspAffordance = PlatformGraspAffordancePtr(new PlatformGraspAffordance())) :
            BimanualAffordance(embodiment, "Bi-G-Pl"),
            platformGraspAffordance(platformGraspAffordance)
        {
        }

    protected:
        Belief evaluateTheta(const PrimitivePair& primitives, unsigned int samplingIndex1, unsigned int samplingIndex2) const override
        {
            // The two samples should allow a platform grasp
            Belief lowerLevelAffordance1 = platformGraspAffordance->getTheta(primitives, samplingIndex1);
            Belief lowerLevelAffordance2 = platformGraspAffordance->getTheta(primitives, samplingIndex2);

            // The distance and orientation between the two samples be appropriate
            Belief b_distance = PropertyBelief::FeasibleBimanualDistance(primitives.first->getSampling(samplingIndex1), primitives.second->getSampling(samplingIndex2), embodiment);

            return lowerLevelAffordance1.logicAnd(lowerLevelAffordance2).logicAnd(b_distance);
        }

        void preEvaluatePrimitive(const PrimitivePtr& primitive) override
        {
            platformGraspAffordance->evaluatePrimitive(primitive);
        }

    protected:
        PlatformGraspAffordancePtr platformGraspAffordance;

    private:
        friend class boost::serialization::access;

        template<class Archive> void serialize(Archive& ar, const unsigned int version)
        {
            ar& boost::serialization::base_object<BimanualAffordance>(*this);
            ar& platformGraspAffordance;
        }
    };

    typedef std::shared_ptr<BimanualPlatformGraspAffordance> BimanualPlatformGraspAffordancePtr;
}

BOOST_CLASS_EXPORT_KEY(AffordanceKit::BimanualPlatformGraspAffordance)
BOOST_CLASS_EXPORT_IMPLEMENT(AffordanceKit::BimanualPlatformGraspAffordance)

#endif


