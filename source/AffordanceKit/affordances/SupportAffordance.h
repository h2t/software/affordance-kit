/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _AffordanceKit_SupportAffordance_H
#define _AffordanceKit_SupportAffordance_H

#include "../UnimanualAffordance.h"
#include "../PropertyBelief.h"
#include "PlatformGraspAffordance.h"

namespace AffordanceKit
{

    class SupportAffordance : public UnimanualAffordance
    {
    public:
        SupportAffordance(const EmbodimentPtr& embodiment = EmbodimentPtr(new Embodiment()),
                          const PlatformGraspAffordancePtr& platformGraspAffordance = PlatformGraspAffordancePtr(new PlatformGraspAffordance())) :
            UnimanualAffordance(embodiment, "Sp"),
            platformGraspAffordance(platformGraspAffordance)
        {
        }

        Belief evaluateTheta(const PrimitivePtr& primitive, unsigned int index) const override
        {
            Belief b_horizontal = PropertyBelief::Horizontal(primitive, index, embodiment);
            Belief b_fixed = PropertyBelief::Fixed(primitive, embodiment);
            Belief lowerLevelAffordance = platformGraspAffordance->getTheta(primitive, index);

            return lowerLevelAffordance.logicAnd(b_horizontal).logicAnd(b_fixed);
        }

        void preEvaluatePrimitive(const PrimitivePtr& primitive) override
        {
            platformGraspAffordance->evaluatePrimitive(primitive);
        }

    protected:
        PlatformGraspAffordancePtr platformGraspAffordance;

    private:
        friend class boost::serialization::access;

        template<class Archive> void serialize(Archive& ar, const unsigned int version)
        {
            ar& boost::serialization::base_object<UnimanualAffordance>(*this);
            ar& platformGraspAffordance;
        }
    };

    typedef std::shared_ptr<SupportAffordance> SupportAffordancePtr;
}

BOOST_CLASS_EXPORT_KEY(AffordanceKit::SupportAffordance)
BOOST_CLASS_EXPORT_IMPLEMENT(AffordanceKit::SupportAffordance)

#endif


