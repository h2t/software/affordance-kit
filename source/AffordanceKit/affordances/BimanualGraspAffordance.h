/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _AffordanceKit_BimanualGraspAffordance_H
#define _AffordanceKit_BimanualGraspAffordance_H

#include "../BimanualAffordance.h"
#include "../PropertyBelief.h"
#include "BimanualPrismaticGraspAffordance.h"
#include "BimanualPlatformGraspAffordance.h"

namespace AffordanceKit
{

    class BimanualGraspAffordance : public BimanualAffordance
    {
    public:
        BimanualGraspAffordance(const EmbodimentPtr& embodiment = EmbodimentPtr(new Embodiment()),
                                const BimanualPrismaticGraspAffordancePtr& prismaticGraspAffordance = BimanualPrismaticGraspAffordancePtr(new BimanualPrismaticGraspAffordance()),
                                const BimanualPlatformGraspAffordancePtr& platformGraspAffordance = BimanualPlatformGraspAffordancePtr(new BimanualPlatformGraspAffordance())) :
            BimanualAffordance(embodiment, "Bi-G"),
            prismaticGraspAffordance(prismaticGraspAffordance),
            platformGraspAffordance(platformGraspAffordance)
        {
        }

    protected:
        Belief evaluateTheta(const PrimitivePair& primitives, unsigned int samplingIndex1, unsigned int samplingIndex2) const override
        {
            if (primitives.primitivesAreDifferent())
            {
                return Belief();
            }

            // The two samples should allow one of the two prismatic grasp types
            Belief b1 = prismaticGraspAffordance->getTheta(primitives.first, samplingIndex1, samplingIndex2);
            Belief b2 = platformGraspAffordance->getTheta(primitives.first, samplingIndex1, samplingIndex2);

            return b1.logicOr(b2);
        }

        void preEvaluatePrimitive(const PrimitivePtr& primitive) override
        {
            prismaticGraspAffordance->evaluatePrimitive(primitive);
            platformGraspAffordance->evaluatePrimitive(primitive);
        }

    protected:
        BimanualPrismaticGraspAffordancePtr prismaticGraspAffordance;
        BimanualPlatformGraspAffordancePtr platformGraspAffordance;

    private:
        friend class boost::serialization::access;

        template<class Archive> void serialize(Archive& ar, const unsigned int version)
        {
            ar& boost::serialization::base_object<BimanualAffordance>(*this);
            ar& prismaticGraspAffordance& platformGraspAffordance;
        }
    };

    typedef std::shared_ptr<BimanualGraspAffordance> BimanualGraspAffordancePtr;
}

BOOST_CLASS_EXPORT_KEY(AffordanceKit::BimanualGraspAffordance)
BOOST_CLASS_EXPORT_IMPLEMENT(AffordanceKit::BimanualGraspAffordance)

#endif




