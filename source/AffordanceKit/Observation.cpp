/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "Observation.h"
#include "Util.h"

using namespace AffordanceKit;

Observation::Observation() :
    zeroMatrix(Eigen::MatrixXf(2, 0))
{
}

Observation::Observation(const PrimitivePtr& primitive, unsigned int index, float result, float certainty, float sigma_pos, float sigma_rot) :
    Observation(primitive, primitive->getSampling(index), result, certainty, sigma_pos, sigma_rot)
{
}


Observation::Observation(const PrimitivePtr& primitive, const PrimitivePtr& otherPrimitive, float result, float certainty, float sigma_pos, float sigma_rot) :
    Observation()
{
    float observationSigmaPosition = sigma_pos;
    float observationSigmaOrientation = sigma_rot;

    Eigen::MatrixXf M(2, primitive->getSamplingSize());
    M.setZero();


    for(unsigned int j = 0; j <  otherPrimitive->getSamplingSize(); j++)
    {
        Eigen::Matrix4f P;
        P = otherPrimitive->getSampling(j);
        for (int i = 0; i < M.cols(); i++)
        {
            float v = evaluateKernel(primitive->getSampling(i), P, observationSigmaPosition, observationSigmaOrientation);
            M.col(i) = Belief(result, 1 - result, certainty * v).toVector();
        }
    }
    theta.reset(new ThetaFunction());
    (*theta)[PrimitivePair(primitive)] = M;
}



Observation::Observation(const PrimitivePtr& primitive, const Eigen::Matrix4f& endEffectorPose, float result, float certainty, float sigma_pos, float sigma_rot) :
    Observation()
{
    float observationSigmaPosition = sigma_pos;
    float observationSigmaOrientation = sigma_rot;

    Eigen::MatrixXf M(2, primitive->getSamplingSize());
    M.setZero();

    for (int i = 0; i < M.cols(); i++)
    {
        float v = evaluateKernel(primitive->getSampling(i), endEffectorPose, observationSigmaPosition, observationSigmaOrientation);
        M.col(i) = Belief(result, 1 - result, certainty * v).toVector();
    }

    theta.reset(new ThetaFunction());
    (*theta)[PrimitivePair(primitive)] = M;
}

Observation::Observation(const ThetaFunctionPtr& theta)
{
    this->theta = theta;
}

AffordanceKit::Belief Observation::query(const PrimitivePair& primitives, unsigned int index) const
{
    if (theta->find(primitives) == theta->end())
    {
        return Belief(0, 0);
    }

    return Belief(theta->at(primitives).col(index));
}

const Eigen::MatrixXf& Observation::getTheta(const PrimitivePtr& primitive) const
{
    if (existsForPrimitive(primitive))
    {
        return theta->at(PrimitivePair(primitive));
    }

    return zeroMatrix;
}

bool Observation::existsForPrimitive(const PrimitivePtr& primitive) const
{
    return (theta->find(PrimitivePair(primitive)) != theta->end());
}

float Observation::evaluateKernel(const Eigen::Matrix4f& pose, const Eigen::Matrix4f& mean, float sigma_pos, float sigma_rot) const
{
    // Use a Gaussian distribution in R^3 for end-effector positions and a von-Mises Fisher distribution in SO(3) for end-effector orientations,
    // both normalized to a maximum value of 1. Refer to Detry 2010 for details
    return evaluateGaussian(pose.block<3, 1>(0, 3), mean.block<3, 1>(0, 3), sigma_pos) * evaluateVonMisesFisher(pose.block<3, 3>(0, 0), mean.block<3, 3>(0, 0), sigma_rot);
}

float Observation::evaluateGaussian(const Eigen::Vector3f& x, const Eigen::Vector3f& mean, float sigma) const
{
    float d = (x - mean).norm() / sigma;
    return exp(-0.5 * d * d);
}

float Observation::evaluateVonMisesFisher(const Eigen::Matrix3f& x, const Eigen::Matrix3f& mean, float sigma) const
{
    Eigen::Quaternionf q_x(x);
    Eigen::Quaternionf q_mean(mean);

    float kappa = 1.0f / sigma;

    // 4D von Mises-Fisher distribution (normalized to a maximum value of 1)
    return exp(kappa * q_x.dot(q_mean)) / exp(kappa);
}

