/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _AffordanceKit_PrimitiveSet_H
#define _AffordanceKit_PrimitiveSet_H

#include "Primitive.h"

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/vector.hpp>

namespace AffordanceKit
{

    class PrimitiveSet : public std::vector<PrimitivePtr>
    {
    public:
        PrimitiveSet();

        /*!
         * \brief Sample the primitives using the given grid parameters
         */
        void sample(float spatialStepSize, int numOrientationalSteps, unsigned int maxPrimitiveSamplingSize = 0);

        PrimitivePtr getPrimitiveById(const std::string& id);
        void getPrimitiveIds(std::vector<std::string>& ids);

        float getTotalArea() const;
        float getTotalSamplingSize() const;

    private:
        friend class boost::serialization::access;

        template<class Archive> void serialize(Archive& ar, const unsigned int version)
        {
            ar& boost::serialization::base_object<std::vector<PrimitivePtr>>(*this);
        }
    };

    typedef std::shared_ptr<PrimitiveSet> PrimitiveSetPtr;

}

#endif


