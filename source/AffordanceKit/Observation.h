/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _AffordanceKit_Observation_H
#define _AffordanceKit_Observation_H

#include "Primitive.h"
#include "Belief.h"
#include "ThetaFunction.h"

namespace AffordanceKit
{
    class Observation
    {
    public:
        Observation();
        Observation(const PrimitivePtr& primitive, const PrimitivePtr& otherPrimitive, float result, float certainty, float sigma_pos = 200, float sigma_rot = M_PI / 4);
        Observation(const PrimitivePtr& primitive, unsigned int index, float result, float certainty, float sigma_pos = 200, float sigma_rot = M_PI / 4);
        Observation(const PrimitivePtr& primitive, const Eigen::Matrix4f& endEffectorPose, float result, float certainty, float sigma_pos = 200, float sigma_rot = M_PI / 4);
        Observation(const ThetaFunctionPtr& theta);

        Belief query(const PrimitivePair& primitives, unsigned int index) const;
        const Eigen::MatrixXf& getTheta(const PrimitivePtr& primitive) const;

        bool existsForPrimitive(const PrimitivePtr& primitive) const;

    protected:
        float evaluateKernel(const Eigen::Matrix4f& pose, const Eigen::Matrix4f& mean, float sigma_pos, float sigma_rot) const;
        float evaluateGaussian(const Eigen::Vector3f& x, const Eigen::Vector3f& mean, float sigma) const;
        float evaluateVonMisesFisher(const Eigen::Matrix3f& x, const Eigen::Matrix3f& mean, float sigma) const;

    protected:
        ThetaFunctionPtr theta;

        Eigen::MatrixXf zeroMatrix;

    private:
        friend class boost::serialization::access;

        template<class Archive> void save(Archive& ar, const unsigned int version) const
        {
            unsigned int sz = theta->size();
            ar& sz;
            for (auto& entry : *theta)
            {
                unsigned int rows = entry.second.rows();
                unsigned int cols = entry.second.cols();

                ar& entry.first.first& entry.first.second& rows& cols;

                for (unsigned int i = 0; i < entry.second.rows(); i++)
                {
                    for (unsigned int j = 0; j < entry.second.cols(); j++)
                    {
                        ar& entry.second(i, j);
                    }
                }
            }
        }

        template<class Archive> void load(Archive& ar, const unsigned int version)
        {
            theta.reset(new ThetaFunction());

            unsigned int sz;
            ar& sz;
            for (unsigned int i = 0; i < sz; i++)
            {
                PrimitivePtr p1, p2;
                unsigned int rows, cols;

                ar& p1& p2& rows& cols;

                Eigen::MatrixXf M(rows, cols);
                for (unsigned int j = 0; j < rows; j++)
                {
                    for (unsigned int k = 0; k < cols; k++)
                    {
                        ar& M(j, k);
                    }
                }

                (*theta)[PrimitivePair(p1, p2)] = M;
            }
        }

        BOOST_SERIALIZATION_SPLIT_MEMBER()
    };

    typedef std::shared_ptr<Observation> ObservationPtr;

}

#endif



