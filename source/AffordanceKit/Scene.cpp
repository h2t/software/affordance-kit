/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "Scene.h"

#include <iostream>
#include <fstream>
#include <iostream>

#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>

using namespace AffordanceKit;

Scene::Scene()
= default;

Scene::Scene(const std::string& filename)
{
    load(filename);
}

Scene::Scene(const PrimitiveSetPtr& primitives, const std::vector<UnimanualAffordancePtr>& unimanualAffordances, const std::vector<BimanualAffordancePtr>& bimanualAffordances) :
    unimanualAffordances(unimanualAffordances),
    bimanualAffordances(bimanualAffordances),
    primitives(primitives)
{
    affordances.reserve(unimanualAffordances.size() + bimanualAffordances.size());

    for (unsigned int i = 0; i < affordances.size(); i++)
    {
        if (i < unimanualAffordances.size())
        {
            affordances[i] = unimanualAffordances[i];
        }
        else
        {
            affordances[i] = bimanualAffordances[i - unimanualAffordances.size()];
        }
    }
}

const std::vector<UnimanualAffordancePtr>& Scene::getUnimanualAffordances() const
{
    return unimanualAffordances;
}

const std::vector<BimanualAffordancePtr>& Scene::getBimanualAffordances() const
{
    return bimanualAffordances;
}

const std::vector<AffordancePtr>& Scene::getAffordances() const
{
    return affordances;
}

std::vector<AffordancePtr> Scene::getAffordancesByName(const std::string& name)
{
    std::vector<AffordancePtr> result;

    for (auto& a : affordances)
    {
        if (a->getName() == name)
        {
            result.push_back(a);
        }
    }

    return result;
}

PrimitiveSetPtr Scene::getPrimitives()
{
    return primitives;
}

const PrimitiveSetPtr Scene::getPrimitives() const
{
    return primitives;
}

void Scene::applyMinExpectedProbabilityFilter(float minExpectedProbability)
{
    std::vector<UnimanualAffordancePtr> filteredUnimanualAffordances;
    std::vector<BimanualAffordancePtr> filteredBimanualAffordances;
    std::vector<AffordancePtr> filteredAffordances;

    for (auto& a : unimanualAffordances)
    {
        for (auto& p : *primitives)
        {
            Belief belief;
            a->getMostCertainPoseForPrimitive(p, belief);

            if (belief.expectedProbability() <= minExpectedProbability)
            {
                a->removeForPrimitive(p);
            }
        }

        if (a->getNumPrimitives() > 0)
        {
            filteredUnimanualAffordances.push_back(a);
            filteredAffordances.push_back(a);
        }
    }

    for (auto& a : bimanualAffordances)
    {
        for (auto& p : *primitives)
        {
            AffordanceKit::Belief belief;
            a->getMostCertainPosesForPrimitive(p, belief);

            if (belief.expectedProbability() <= minExpectedProbability)
            {
                a->removeForPrimitive(p);
            }
        }

        if (a->getNumPrimitives() > 0)
        {
            filteredBimanualAffordances.push_back(a);
            filteredAffordances.push_back(a);
        }
    }

    unimanualAffordances = filteredUnimanualAffordances;
    bimanualAffordances = filteredBimanualAffordances;
    affordances = filteredAffordances;
}

void Scene::save(const std::string& filename) const
{
    std::ofstream ofs(filename);

    boost::iostreams::filtering_ostream compress_os;
    compress_os.push(boost::iostreams::zlib_compressor());
    compress_os.push(ofs);

    boost::archive::binary_oarchive oa(compress_os);
    oa << *this;
}

void Scene::load(const std::string& filename)
{
    std::ifstream ifs(filename);

    boost::iostreams::filtering_istream compress_is;
    compress_is.push(boost::iostreams::zlib_decompressor());
    compress_is.push(ifs);

    boost::archive::binary_iarchive ia(compress_is);
    ia >> *this;
}

float Scene::getTotalUncertainty(const std::string& affordanceName)
{
    float uncertaintySum = 0;
    unsigned int totalPossibleUncertainty = 0;

    for (auto& a : unimanualAffordances)
    {
        if (affordanceName != "" && a->getName() != affordanceName)
        {
            continue;
        }

        for (auto& p : *primitives)
        {
            if (!a->existsForPrimitive(p))
            {
                continue;
            }

            for (unsigned int i = 0; i < p->getSamplingSize(); i++)
            {
                Belief b = a->getTheta(PrimitivePair(p), i);
                uncertaintySum += b.either();
                totalPossibleUncertainty++;
            }
        }
    }

    if (bimanualAffordances.size() > 0)
    {
        std::cout << "Warning: getTotalUncertainty is not yet implemented for bimanual affordances" << std::endl;
    }


    if (totalPossibleUncertainty > 0)
    {
        return uncertaintySum / totalPossibleUncertainty;
    }
    else
    {
        std::cout << "Warning: unable to compute getTotalUncertainty()." << std::endl;
        return 0;
    }

}

float Scene::getPositiveDecisionRate(const std::string& affordanceName)
{
    unsigned int positiveDecisions = 0;
    unsigned int total = 0;

    for (auto& a : unimanualAffordances)
    {
        if (affordanceName != "" && a->getName() != affordanceName)
        {
            continue;
        }

        for (auto& p : *primitives)
        {
            if (!a->existsForPrimitive(p))
            {
                continue;
            }

            for (unsigned int i = 0; i < p->getSamplingSize(); i++)
            {
                Belief b = a->getTheta(PrimitivePair(p), i);

                positiveDecisions += (b.expectedProbability() >= 0.5);
                total++;
            }
        }
    }

    if (bimanualAffordances.size() > 0)
    {
        std::cout << "Warning: getPositiveDecisionRate is not yet implemented for bimanual affordances" << std::endl;
    }

    if (total > 0)
    {
        return positiveDecisions / ((float)total);
    }
    else
    {
        std::cout << "Warning: unable to compute positive decision rate." << std::endl;
        return 0;
    }
}

float Scene::getNegativeDecisionRate(const std::string& affordanceName)
{
    return 1 - getPositiveDecisionRate(affordanceName);
}

float Scene::getTotalConflict(const std::string& affordanceName)
{
    float conflictSum = 0;
    float totalPossibleConflict = 0;

    for (auto& a : unimanualAffordances)
    {
        if (affordanceName != "" && a->getName() != affordanceName)
        {
            continue;
        }

        for (auto& p : *primitives)
        {
            if (!a->existsForPrimitive(p))
            {
                continue;
            }

            for (unsigned int i = 0; i < p->getSamplingSize(); i++)
            {
                Belief b = a->getTheta(PrimitivePair(p), i);
                conflictSum += b.exists() * b.nexists();
                totalPossibleConflict += 0.25;
            }
        }
    }

    if (bimanualAffordances.size() > 0)
    {
        std::cout << "Warning: getTotalUncertainty is not yet implemented for bimanual affordances" << std::endl;
    }

    if (totalPossibleConflict > 0)
    {
        return conflictSum / totalPossibleConflict;
    }
    else
    {
        return 0;
    }
}
