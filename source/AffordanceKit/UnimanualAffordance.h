/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _AffordanceKit_UnimanualAffordance_H
#define _AffordanceKit_UnimanualAffordance_H

#include "Affordance.h"
#include "Util.h"

#include <boost/serialization/export.hpp>
#include <boost/serialization/base_object.hpp>

namespace AffordanceKit
{
    class UnimanualAffordance : public Affordance
    {
    public:
        UnimanualAffordance(const EmbodimentPtr& embodiment, const std::string& name, float affordanceExtractionCertainty = 1.0f);
        UnimanualAffordance();

        void evaluatePrimitiveSet(const PrimitiveSetPtr& primitiveSet) override;
        void evaluatePrimitive(const PrimitivePtr& primitive) override;

        virtual Belief evaluateTheta(const PrimitivePtr& primitive, unsigned int index) const = 0;

        Eigen::Matrix4f getMostCertainPoseForPrimitive(const PrimitivePtr& primitive, Belief& value);

    private:
        friend class boost::serialization::access;

        template<class Archive> void serialize(Archive& ar, const unsigned int version)
        {
            ar& boost::serialization::base_object<Affordance>(*this);
        }
    };

    typedef std::shared_ptr<UnimanualAffordance> UnimanualAffordancePtr;

}

BOOST_CLASS_EXPORT_KEY(AffordanceKit::UnimanualAffordance)

#endif

