/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "BimanualAffordance.h"

#include <iostream>

#include "primitives/Box.h"

using namespace AffordanceKit;

BimanualAffordance::BimanualAffordance(const EmbodimentPtr& embodiment, const std::string& name, float affordanceExtractionCertainty) :
    Affordance(embodiment, name, affordanceExtractionCertainty)
{
    reset();
}

BimanualAffordance::BimanualAffordance() :
    Affordance()
{
    reset();
}

void BimanualAffordance::evaluatePrimitiveSet(const PrimitiveSetPtr& primitiveSet)
{
    unsigned int size = primitiveSet->size();

    // First extract bimanual affordances for each primitive individually
    for (unsigned int i = 0; i < size; i++)
    {
        std::cout << "Bimanual affordance: evaluating primitive " << i << std::endl;
        evaluatePrimitive(primitiveSet->at(i));
    }

#if 0
    // Now extract bimanual affordances for pairs of primitives
    for (unsigned int i = 0; i < size; i++)
    {
        for (unsigned int j = i + 1; j < size; j++)
        {
            evaluatePrimitivePair(primitiveSet->at(i), primitiveSet->at(j));
        }
    }
#endif
}

void BimanualAffordance::evaluatePrimitive(const PrimitivePtr& primitive)
{
    auto pair = PrimitivePair(primitive);
    if (theta->find(pair) != theta->end())
    {
        // No re-evaluation
        return;
    }

    // Temporary: Reduce bimanual affordances to box primitives
    if (!std::dynamic_pointer_cast<Box>(primitive))
    {
        return;
    }

    preEvaluatePrimitive(primitive);

    unsigned int size = primitive->getSamplingSize();

    if (size * size > 50000000)
    {
        std::cout << "Primitive too large" << std::endl;
        return;
    }

    std::cout << "Creating matrix of size 4x" << (size * size) << std::endl;
    (*theta)[pair] = Eigen::MatrixXf(2, size * size);
    Eigen::MatrixXf& beliefs = (*theta)[pair];

    #pragma omp parallel for
    for (unsigned int index1 = 0; index1 < size; index1++)
    {
        for (unsigned int index2 = 0; index2 < size; index2++)
        {
            beliefs.col(index1 * size + index2) = evaluateTheta(pair, index1, index2).toVector();
        }
    }
}

std::pair<Eigen::Matrix4f, Eigen::Matrix4f> BimanualAffordance::getMostCertainPosesForPrimitive(const PrimitivePtr& primitive, AffordanceKit::Belief& value)
{
    auto pair = PrimitivePair(primitive, primitive);
    if (theta->find(pair) == theta->end() || theta->at(pair).size() == 0)
    {
        value = AffordanceKit::Belief(0, 0);
        return std::pair<Eigen::Matrix4f, Eigen::Matrix4f>(Eigen::Matrix4f::Zero(), Eigen::Matrix4f::Zero());
    }

    unsigned int i1 = 0;
    unsigned int i2 = 0;
    float max = -std::numeric_limits<float>::infinity();
    AffordanceKit::Belief maxval;
    unsigned int size = theta->at(pair).cols();
    unsigned int samplingSize = primitive->getSamplingSize();
    for (unsigned int i = 0; i < size; i++)
    {
        Belief v(theta->at(pair).col(i));
        if (v.expectedProbability() > max)
        {
            i1 = i / samplingSize;
            i2 = i % samplingSize;
            max = v.expectedProbability();
            maxval = v;
        }
    }

    value = maxval;
    return std::pair<Eigen::Matrix4f, Eigen::Matrix4f>(primitive->getSampling(i1), primitive->getSampling(i2));
}

void BimanualAffordance::evaluatePrimitivePair(const PrimitivePair& primitives)
{
    preEvaluatePrimitive(primitives.first);
    preEvaluatePrimitive(primitives.second);

    unsigned int size1 = primitives.first->getSamplingSize();
    unsigned int size2 = primitives.second->getSamplingSize();

    Eigen::MatrixXf beliefs(2, size1 * size2);

    #pragma omp parallel for
    for (unsigned int index1 = 0; index1 < size1; index1++)
    {
        for (unsigned int index2 = 0; index2 < size2; index2++)
        {
            Belief value = evaluateTheta(primitives, index1, index2);
            beliefs.col(index1 * size2 + index2) = value.toVector();
        }
    }
}

BOOST_CLASS_EXPORT_IMPLEMENT(AffordanceKit::BimanualAffordance)
