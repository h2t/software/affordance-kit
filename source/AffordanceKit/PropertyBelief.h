/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _AffordanceKit_PropertyBelief_H
#define _AffordanceKit_PropertyBelief_H

#include "Belief.h"
#include "Primitive.h"
#include "Embodiment.h"

namespace AffordanceKit
{
    class PropertyBelief
    {
    public:
        enum ThresholdDomain
        {
            eSpatial = 0,
            eRotational = 1
        };

    public:
        static float SigmoidSpatial(float beta, float x)
        {
            int index = std::max(0, std::min((sigmoidLookupTableUpperBound - sigmoidLookupTableLowerBound) * sigmoidLookupTableResolution - 1, (int)((x - beta - sigmoidLookupTableLowerBound) * sigmoidLookupTableResolution)));
            return sigmoidLookupTable(eSpatial, index);
        }

        static float SigmoidRotational(float beta, float x)
        {
            int index = std::max(0, std::min((sigmoidLookupTableUpperBound - sigmoidLookupTableLowerBound) * sigmoidLookupTableResolution - 1, (int)((x - beta - sigmoidLookupTableLowerBound) * sigmoidLookupTableResolution)));
            return sigmoidLookupTable(eRotational, index);
        }

        static Belief GreaterThanThresholdBelief(float value, float threshold, ThresholdDomain domain, float certainty = 1.0f)
        {
            float v;

            switch (domain)
            {
                case eSpatial:
                    v = SigmoidSpatial(threshold, value);
                    break;

                case eRotational:
                    v = SigmoidRotational(threshold, value);
                    break;
            }

            return Belief(v, 1.0f - v, certainty);
        }

        static Belief LesserThanThresholdBelief(float value, float threshold, ThresholdDomain domain, float certainty = 1.0f)
        {
            float v;

            switch (domain)
            {
                case eSpatial:
                    v = SigmoidSpatial(threshold, value);
                    break;

                case eRotational:
                    v = SigmoidRotational(threshold, value);
                    break;
            }

            return Belief(1.0f - v, v, certainty);
        }

        static Belief InThresholdIntervalBelief(float value, float threshold_low, float threshold_high, ThresholdDomain domain, float certainty = 1.0f)
        {
            float v;

            switch (domain)
            {
                case eSpatial:
                    v = SigmoidSpatial(threshold_low, value) * (1.0f - SigmoidSpatial(threshold_high, value));
                    break;

                case eRotational:
                    v = SigmoidRotational(threshold_low, value) * (1.0f - SigmoidRotational(threshold_high, value));
                    break;
            }

            return Belief(v, 1.0f - v, certainty);
        }

        static Eigen::MatrixXf GenerateSigmoidLookupTable();

    public:
        static Belief Horizontal(const PrimitivePtr& primitive, unsigned int index, const EmbodimentPtr& embodiment, float certainty = 1.0f);
        static Belief Horizontal(const Eigen::Vector3f& direction, const EmbodimentPtr& embodiment, float certainty = 1.0f);
        static Belief Vertical(const Eigen::Matrix4f& pose, const EmbodimentPtr& embodiment, float certainty = 1.0f);
        static Belief Circular(const PrimitivePtr& primitive, float certainty = 1.0f);
        static Belief Fixed(const PrimitivePtr& primitive, const EmbodimentPtr& embodiment, float certainty = 1.0f);
        static Belief Movable(const PrimitivePtr& primitive, const EmbodimentPtr& embodiment, float certainty = 1.0f);

        // Bimanual properties
        static Belief FeasibleBimanualDistance(const Eigen::Matrix4f& pose1, const Eigen::Matrix4f& pose2, const EmbodimentPtr& embodiment, float certainty = 1.0f);
        static Belief FeasibleBimanualOrientation(const Eigen::Matrix4f& pose1, const Eigen::Matrix4f& pose2, const EmbodimentPtr& embodiment, float certainty = 1.0f);
        static Belief Opposed(const Eigen::Matrix4f& pose1, const Eigen::Matrix4f& pose2, const EmbodimentPtr& embodiment, float certainty = 1.0f);
        static Belief Aligned(const Eigen::Matrix4f& pose1, const Eigen::Matrix4f& pose2, const EmbodimentPtr& embodiment, float certainty = 1.0f);

    protected:
        static Eigen::MatrixXf sigmoidLookupTable;

        static Eigen::Vector3f up;

        static int sigmoidLookupTableLowerBound;
        static int sigmoidLookupTableUpperBound;
        static int sigmoidLookupTableResolution;

        static float lambdaSpatial;
        static float lambdaRotational;
    };
}

#endif


