/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Eren E. Aksoy ( eren dot aksoy at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _AffordanceKit_PrimitiveExtractor_H
#define _AffordanceKit_PrimitiveExtractor_H

#include "PrimitiveSet.h"

#include <iostream>
#include <vector>

#include <opencv2/opencv.hpp>
#include <boost/math/special_functions/fpclassify.hpp>
#include <cstdlib>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/search/search.h>
#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <pcl/octree/octree.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/conditional_removal.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/point_cloud.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/features/normal_3d.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/sample_consensus/sac_model_plane.h>
#include <pcl/sample_consensus/sac_model_circle.h>
#include <pcl/sample_consensus/ransac.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/surface/concave_hull.h>
#include <pcl/common/io.h>
#include <pcl/io/io.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/features/integral_image_normal.h>
#include <pcl/segmentation/planar_region.h>
#include <pcl/segmentation/organized_connected_component_segmentation.h>
#include <pcl/geometry/polygon_operations.h>
#pragma GCC diagnostic pop

typedef pcl::PointXYZ PointO;
typedef pcl::PointCloud<PointO>::Ptr PointOPtr;
typedef pcl::PointXYZL PointL;
typedef pcl::PointCloud<PointL>::Ptr PointLPtr;
typedef pcl::PointXYZRGB PointT;
typedef pcl::PointCloud<PointT>::Ptr PointTPtr;

namespace armarx
{
    class PrimitiveExtractor;
}

namespace AffordanceKit
{

    class PrimitiveModel
    {
    public:
        double score;
        std::string modelType;
        pcl::ModelCoefficients::Ptr modelCoefficients;
        PointLPtr inliers;
        PointLPtr graspPoints;
    };
    typedef std::shared_ptr<PrimitiveModel> PrimitiveModelPtr;

    class PrimitiveExtractor
    {

        friend class ::armarx::PrimitiveExtractor;

    private:
        PointLPtr inputCloudPtr;
        PointLPtr inlierCloudPtr;
        PointLPtr graspPoints;

        size_t minSegmentSize;
        size_t maxSegmentSize;
        std::map<uint32_t, pcl::PointIndices> labelMap;

        uint32_t newLabel;

        void getLabelMap();
        PrimitiveModelPtr computeModel(const PointLPtr& segmentPointCloud, const pcl::PointCloud<pcl::Normal>::Ptr& normals);
        pcl::PointCloud<pcl::Normal>::Ptr computeNormals(const PointLPtr& segmentPointCloud);
        PointLPtr computeOutliers(const PointLPtr& segmentPointCloud, const pcl::PointCloud<pcl::Normal>::Ptr& normals, PrimitiveModelPtr& model);
        void computeEuclideanClusters();
        void computeEuclideanClusters(const PointLPtr& inliers, std::vector<pcl::PointIndices>& segments);
        void computeCirclesFromGraspPoints();
        void updateInliersAndCoefficients(PrimitiveModelPtr& model);
        void runIterativePrimitiveExtractor();
        void mergeSimilarPrimitives();

        PrimitiveModelPtr calculatePlaneCoefficients(const PointLPtr& segmentPointCloud, const pcl::PointCloud<pcl::Normal>::Ptr& normals);
        PrimitiveModelPtr calculateCylinderCoefficients(const PointLPtr& segmentPointCloud, const pcl::PointCloud<pcl::Normal>::Ptr& normals);
        PrimitiveModelPtr calculateSphereCoefficients(const PointLPtr& segmentPointCloud, const pcl::PointCloud<pcl::Normal>::Ptr& normals);
        bool computeNeighborhoodRelation(PointLPtr cloud_i, PointLPtr cloud_j);

        std::vector<PointLPtr> detectedPlaneInliers;
        std::vector<PointLPtr> detectedCylinderInliers;
        std::vector<PointLPtr> detectedSphereInliers;

        std::vector<PointLPtr> detectedPlaneGraspPoints;
        std::vector<PointLPtr> detectedCylinderGraspPoints;
        std::vector<PointLPtr> detectedSphereGraspPoints;

        std::vector<PointLPtr> mergedPlaneInliers;

        std::vector<pcl::ModelCoefficients::Ptr> detectedPlaneCoeff;
        std::vector<pcl::ModelCoefficients::Ptr> detectedCylinderCoeff;
        std::vector<pcl::ModelCoefficients::Ptr> detectedSphereCoeff;

        std::vector<float> detectedCircularityProbabilities;
        std::vector<pcl::ModelCoefficients::Ptr> detectedCircleCoeff;

        std::vector<PointLPtr> euclideanPlaneInliers;
        std::vector<PointLPtr> euclideanCylinderInliers;
        std::vector<PointLPtr> euclideanSphereInliers;

        std::vector<PointLPtr> euclideanPlaneGraspPoints;
        std::vector<PointLPtr> euclideanCylinderGraspPoints;
        std::vector<PointLPtr> euclideanSphereGraspPoints;

        std::vector<pcl::ModelCoefficients::Ptr> euclideanPlaneCoeff;
        std::vector<pcl::ModelCoefficients::Ptr> euclideanCylinderCoeff;
        std::vector<pcl::ModelCoefficients::Ptr> euclideanSphereCoeff;

        bool verbose;
        double planeMaxIteration;
        float planeDistanceThreshold;
        float planeNormalDistanceWeight;
        float circleDistanceThreshold;

        double cylinderMaxIteration;
        float cylinderDistanceThreshold;
        float cylinderRadiusLimit;

        double sphereMaxIteration;
        float sphereDistanceThreshold;
        float sphereNormalDistanceWeight;

        float euclideanClusteringTolerance;
        float outlierThreshold;

    public:
        PrimitiveExtractor();

        PointLPtr& computeScenePrimitives(PointLPtr& CloudPtr);

        PointLPtr& getDetectedGraspPoints()
        {
            return graspPoints;
        }

        const std::vector<PointLPtr>& getPlaneInliers() const
        {
            return detectedPlaneInliers;
        }

        const std::vector<PointLPtr>& getCylinderInliers() const
        {
            return detectedCylinderInliers;
        }

        const std::vector<PointLPtr>& getSphereInliers() const
        {
            return detectedSphereInliers;
        }

        const std::vector<PointLPtr>& getPlaneGraspPoints() const
        {
            return detectedPlaneGraspPoints;
        }

        const std::vector<PointLPtr>& getCylinderGraspPoints() const
        {
            return detectedCylinderGraspPoints;
        }

        const std::vector<PointLPtr>& getSphereGraspPoints() const
        {
            return detectedSphereGraspPoints;
        }

        const std::vector<PointLPtr>& getMergedPlaneInliers() const
        {
            return mergedPlaneInliers;
        }

        const std::vector<pcl::ModelCoefficients::Ptr>& getPlaneCoefficients() const
        {
            return detectedPlaneCoeff;
        }

        const std::vector<pcl::ModelCoefficients::Ptr>& getCylinderCoefficients() const
        {
            return detectedCylinderCoeff;
        }

        const std::vector<pcl::ModelCoefficients::Ptr>& getSphereCoefficients() const
        {
            return detectedSphereCoeff;
        }

        const std::vector<float>& getCircularityProbabilities() const
        {
            return detectedCircularityProbabilities;
        }

        const std::vector<pcl::ModelCoefficients::Ptr>& getCircleCoefficients() const
        {
            return detectedCircleCoeff;
        }

        PrimitiveSetPtr getPrimitiveSet(int spatialStepSize, int numOrientationalSteps, long long originalTimestamp);

        void updateParameters(bool verbose, size_t minSegmentSize, size_t maxSegmentSize, double pMaxIter, float pDistThres, float pNorDist, double cMaxIter, float cDistThres, float cRadLim, double sMaxIter, float sDistThres, float sNorDist, float  euclClusTolerance, float outlThres, float circleDistThres);
    };
    using PrimitiveExtractorPtr = std::shared_ptr<PrimitiveExtractor>;
}

#endif
