/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "Cylinder.h"
#include "../Util.h"

using namespace AffordanceKit;

Cylinder::Cylinder() :
    basePoint(Eigen::Vector3f(0, 0, 0)),
    direction(Eigen::Vector3f(1, 0, 0)),
    length(0),
    radius(0)
{
}

Cylinder::Cylinder(const Eigen::Vector3f& basePoint, const Eigen::Vector3f& direction, float length, float radius) :
    basePoint(basePoint),
    direction(direction.normalized()),
    length(length),
    radius(radius)
{
}

Eigen::Vector3f Cylinder::getBasePoint()
{
    return basePoint;
}

Eigen::Vector3f Cylinder::getDirection()
{
    return direction;
}

float Cylinder::getLength()
{
    return length;
}

float Cylinder::getRadius()
{
    return radius;
}

PrimitiveSubVolumePtr Cylinder::getVolumeInDirection(const Eigen::Matrix4f& pose, float depth, float epsilon)
{
    // TODO: This is a dummy implementation
    float min_x, max_x, min_y, max_y, min_z, max_z;

    min_z = 0;
    max_z = 2 * radius;

    Eigen::Vector3f p = pose.block<3, 1>(0, 3) - basePoint;
    float l_p = p.dot(direction) / direction.norm();

    float l_p_min = -length / 2 - l_p;
    float l_p_max = length / 2 - l_p;

    if (fabs(pose.block<3, 1>(0, 0).dot(direction)) < epsilon)
    {
        // y points in cylinder direction
        min_x = -radius;
        max_x = radius;

        min_y = std::min(l_p_min, l_p_max);
        max_y = std::max(l_p_min, l_p_max);
    }
    else
    {
        // x points in cylinder direction
        min_y = -radius;
        max_y = radius;

        min_x = std::min(l_p_min, l_p_max);
        max_x = std::max(l_p_min, l_p_max);
    }

    return PrimitiveSubVolumePtr(new PrimitiveSubVolume(min_x, max_x, min_y, max_y, min_z, max_z));
}

Eigen::MatrixXf Cylinder::sample_impl(float spatialStepSize, int numOrientationalSteps) const
{
    Eigen::MatrixXf S(4, 0);

    for (float t = 0; t < length / 2; t += spatialStepSize)
    {
        Eigen::MatrixXf A = S;
        Eigen::MatrixXf B = sampleSlice(basePoint + t * direction, spatialStepSize, numOrientationalSteps);
        Eigen::MatrixXf C = sampleSlice(basePoint - t * direction, spatialStepSize, numOrientationalSteps);

        S = Eigen::MatrixXf(4, A.cols() + B.cols() + C.cols());
        S << A, B, C;
    }

    return S;
}

Eigen::Vector3f Cylinder::getCenter() const
{
    return basePoint;
}

Eigen::Vector3f Cylinder::getDimensions() const
{
    return Eigen::Vector3f(length, 2 * radius, 2 * radius);
}

float Cylinder::isCircular()
{
    return 0;
}

Eigen::MatrixXf Cylinder::sampleSlice(const Eigen::Vector3f& p, int spatialStepSize, int numOrientationalSteps) const
{
    Eigen::Matrix4f P = Eigen::Matrix4f::Identity();
    P.block<3, 1>(0, 3) = p;

    Eigen::Vector3f arbitraryAxis;
    if ((direction - Eigen::Vector3f::UnitZ()).norm() > (direction - Eigen::Vector3f::UnitY()).norm())
    {
        arbitraryAxis = Eigen::Vector3f::UnitZ();
    }
    else
    {
        arbitraryAxis = Eigen::Vector3f::UnitX();
    }

    Eigen::Vector3f z = direction.cross(arbitraryAxis);
    P.block<3, 1>(0, 0) = -z.cross(direction);
    P.block<3, 1>(0, 1) = direction;
    P.block<3, 1>(0, 2) = z;

    // Include an offset for proper visualization
    Eigen::Matrix4f T = Eigen::Matrix4f::Identity();
    T(2, 3) = -radius - 5;

    Eigen::Matrix4f R1 = Eigen::Matrix4f::Identity();
    Eigen::Matrix4f R2 = Eigen::Matrix4f::Identity();

    // Sample at least in four directions
    int numSamplings = std::max(2 * M_PI * radius / spatialStepSize, 4.0);

    Eigen::MatrixXf result(4, 4 * numSamplings * numOrientationalSteps);

    for (int i = 0; i < numSamplings; i++)
    {
        R1.block<3, 3>(0, 0) = Eigen::Matrix3f(Eigen::AngleAxisf(i * 2 * M_PI / numSamplings, Eigen::Vector3f::UnitY()));

        for (int j = 0; j < numOrientationalSteps; j++)
        {
            R2.block<3, 3>(0, 0) = Eigen::Matrix3f(Eigen::AngleAxisf(j * 2 * M_PI / numOrientationalSteps, Eigen::Vector3f::UnitZ()));
            result.block<4, 4>(0, 4 * (i * numOrientationalSteps + j)) = P * R1 * T * R2;
        }
    }

    return result;
}

float AffordanceKit::Cylinder::getArea() const
{
    return 2 * M_PI * radius * radius + 2 * M_PI * radius * length;
}

template<class Archive> void AffordanceKit::Cylinder::save(Archive& ar, const unsigned int version) const
{
    ar& boost::serialization::base_object<Primitive>(*this);

    ar& basePoint.x() & basePoint.y() & basePoint.z();
    ar& direction.x() & direction.y() & direction.z();
    ar& length& radius;
}

template<class Archive> void AffordanceKit::Cylinder::load(Archive& ar, const unsigned int version)
{
    ar& boost::serialization::base_object<Primitive>(*this);

    float bx, by, bz, dx, dy, dz;
    ar& bx& by& bz& dx& dy& dz& length& radius;

    basePoint = Eigen::Vector3f(bx, by, bz);
    direction = Eigen::Vector3f(dx, dy, dz);
}

BOOST_CLASS_EXPORT_IMPLEMENT(AffordanceKit::Cylinder)
