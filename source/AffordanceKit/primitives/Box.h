/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _AffordanceKit_Sphere_Box_H
#define _AffordanceKit_Sphere_Box_H

#include "../Primitive.h"
#include "Plane.h"

#include <Eigen/Eigen>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/export.hpp>

namespace AffordanceKit
{
    class Box  : public Primitive
    {
    public:
        Box();
        Box(const Eigen::Matrix4f& pose, const Eigen::Vector3f& dimensions);

        PrimitiveSubVolumePtr getVolumeInDirection(const Eigen::Matrix4f& pose, float depth = -1, float epsilon = 0.1) override;

        Eigen::Vector3f getCenter() const override;
        Eigen::Matrix4f getPose() const;
        Eigen::Vector3f getDimensions() const override;
        float getArea() const override;

        float isCircular() override;

    protected:
        Eigen::MatrixXf sample_impl(float spatialStepSize, int numOrientationalSteps) const override;
        void initialize();

    protected:
        Eigen::Matrix4f pose;
        Eigen::Vector3f dimensions;
        std::vector<PlanePtr> sides;

    private:
        friend class boost::serialization::access;

        template<class Archive> void save(Archive& ar, const unsigned int version) const;
        template<class Archive> void load(Archive& ar, const unsigned int version);

        BOOST_SERIALIZATION_SPLIT_MEMBER()
    };

    typedef std::shared_ptr<Box> BoxPtr;
}

BOOST_CLASS_EXPORT_KEY(AffordanceKit::Box)

#endif
