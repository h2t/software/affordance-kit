/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Box.h"

using namespace AffordanceKit;

Box::Box() :
    Box(Eigen::Matrix4f::Identity(), Eigen::Vector3f::Zero())
{
}

Box::Box(const Eigen::Matrix4f& pose, const Eigen::Vector3f& dimensions) :
    pose(pose),
    dimensions(dimensions)
{
    initialize();
}

PrimitiveSubVolumePtr Box::getVolumeInDirection(const Eigen::Matrix4f& pose, float depth, float epsilon)
{
    for (auto& side : sides)
    {
        if (side->isInPlane(pose.block<3, 1>(0, 3)))
        {
            return side->getVolumeInDirection(pose, depth, epsilon);
        }
    }

    return PrimitiveSubVolumePtr(new PrimitiveSubVolume());
}

Eigen::Vector3f Box::getCenter() const
{
    return pose.block<3, 1>(0, 3);
}

Eigen::MatrixXf Box::sample_impl(float spatialStepSize, int numOrientationalSteps) const
{
    std::vector<Eigen::Matrix4f> sampling;
    for (auto& side : sides)
    {
        side->samplePlane(spatialStepSize, numOrientationalSteps, sampling, false);
    }

    Eigen::MatrixXf S(4, sampling.size() * 4);
    for (unsigned int i = 0; i < sampling.size(); i++)
    {
        S.block<4, 4>(0, 4 * i) = sampling[i];
    }

    return S;
}

void Box::initialize()
{
    std::vector<std::vector<Eigen::Vector3f>> s;
    s.resize(6);

    std::vector<Eigen::Vector3f> centers;
    centers.resize(6);

    s[0].push_back(pose.block<3, 1>(0, 3) + (dimensions.x() / 2) * pose.block<3, 1>(0, 0) + (dimensions.y() / 2) * pose.block<3, 1>(0, 1) + (dimensions.z() / 2) * pose.block<3, 1>(0, 2));
    s[0].push_back(pose.block<3, 1>(0, 3) + (dimensions.x() / 2) * pose.block<3, 1>(0, 0) + (dimensions.y() / 2) * pose.block<3, 1>(0, 1) - (dimensions.z() / 2) * pose.block<3, 1>(0, 2));
    s[0].push_back(pose.block<3, 1>(0, 3) + (dimensions.x() / 2) * pose.block<3, 1>(0, 0) - (dimensions.y() / 2) * pose.block<3, 1>(0, 1) + (dimensions.z() / 2) * pose.block<3, 1>(0, 2));
    s[0].push_back(pose.block<3, 1>(0, 3) + (dimensions.x() / 2) * pose.block<3, 1>(0, 0) - (dimensions.y() / 2) * pose.block<3, 1>(0, 1) - (dimensions.z() / 2) * pose.block<3, 1>(0, 2));
    centers[0] = pose.block<3, 1>(0, 3) + (dimensions.x() / 2) * pose.block<3, 1>(0, 0);

    s[1].push_back(pose.block<3, 1>(0, 3) - (dimensions.x() / 2) * pose.block<3, 1>(0, 0) + (dimensions.y() / 2) * pose.block<3, 1>(0, 1) + (dimensions.z() / 2) * pose.block<3, 1>(0, 2));
    s[1].push_back(pose.block<3, 1>(0, 3) - (dimensions.x() / 2) * pose.block<3, 1>(0, 0) + (dimensions.y() / 2) * pose.block<3, 1>(0, 1) - (dimensions.z() / 2) * pose.block<3, 1>(0, 2));
    s[1].push_back(pose.block<3, 1>(0, 3) - (dimensions.x() / 2) * pose.block<3, 1>(0, 0) - (dimensions.y() / 2) * pose.block<3, 1>(0, 1) + (dimensions.z() / 2) * pose.block<3, 1>(0, 2));
    s[1].push_back(pose.block<3, 1>(0, 3) - (dimensions.x() / 2) * pose.block<3, 1>(0, 0) - (dimensions.y() / 2) * pose.block<3, 1>(0, 1) - (dimensions.z() / 2) * pose.block<3, 1>(0, 2));
    centers[1] = pose.block<3, 1>(0, 3) - (dimensions.x() / 2) * pose.block<3, 1>(0, 0);

    s[2].push_back(pose.block<3, 1>(0, 3) + (dimensions.x() / 2) * pose.block<3, 1>(0, 0) + (dimensions.y() / 2) * pose.block<3, 1>(0, 1) + (dimensions.z() / 2) * pose.block<3, 1>(0, 2));
    s[2].push_back(pose.block<3, 1>(0, 3) + (dimensions.x() / 2) * pose.block<3, 1>(0, 0) + (dimensions.y() / 2) * pose.block<3, 1>(0, 1) - (dimensions.z() / 2) * pose.block<3, 1>(0, 2));
    s[2].push_back(pose.block<3, 1>(0, 3) - (dimensions.x() / 2) * pose.block<3, 1>(0, 0) + (dimensions.y() / 2) * pose.block<3, 1>(0, 1) + (dimensions.z() / 2) * pose.block<3, 1>(0, 2));
    s[2].push_back(pose.block<3, 1>(0, 3) - (dimensions.x() / 2) * pose.block<3, 1>(0, 0) + (dimensions.y() / 2) * pose.block<3, 1>(0, 1) - (dimensions.z() / 2) * pose.block<3, 1>(0, 2));
    centers[2] = pose.block<3, 1>(0, 3) + (dimensions.y() / 2) * pose.block<3, 1>(0, 1);

    s[3].push_back(pose.block<3, 1>(0, 3) + (dimensions.x() / 2) * pose.block<3, 1>(0, 0) - (dimensions.y() / 2) * pose.block<3, 1>(0, 1) + (dimensions.z() / 2) * pose.block<3, 1>(0, 2));
    s[3].push_back(pose.block<3, 1>(0, 3) + (dimensions.x() / 2) * pose.block<3, 1>(0, 0) - (dimensions.y() / 2) * pose.block<3, 1>(0, 1) - (dimensions.z() / 2) * pose.block<3, 1>(0, 2));
    s[3].push_back(pose.block<3, 1>(0, 3) - (dimensions.x() / 2) * pose.block<3, 1>(0, 0) - (dimensions.y() / 2) * pose.block<3, 1>(0, 1) + (dimensions.z() / 2) * pose.block<3, 1>(0, 2));
    s[3].push_back(pose.block<3, 1>(0, 3) - (dimensions.x() / 2) * pose.block<3, 1>(0, 0) - (dimensions.y() / 2) * pose.block<3, 1>(0, 1) - (dimensions.z() / 2) * pose.block<3, 1>(0, 2));
    centers[3] = pose.block<3, 1>(0, 3) - (dimensions.y() / 2) * pose.block<3, 1>(0, 1);

    s[4].push_back(pose.block<3, 1>(0, 3) + (dimensions.x() / 2) * pose.block<3, 1>(0, 0) + (dimensions.y() / 2) * pose.block<3, 1>(0, 1) + (dimensions.z() / 2) * pose.block<3, 1>(0, 2));
    s[4].push_back(pose.block<3, 1>(0, 3) + (dimensions.x() / 2) * pose.block<3, 1>(0, 0) - (dimensions.y() / 2) * pose.block<3, 1>(0, 1) + (dimensions.z() / 2) * pose.block<3, 1>(0, 2));
    s[4].push_back(pose.block<3, 1>(0, 3) - (dimensions.x() / 2) * pose.block<3, 1>(0, 0) + (dimensions.y() / 2) * pose.block<3, 1>(0, 1) + (dimensions.z() / 2) * pose.block<3, 1>(0, 2));
    s[4].push_back(pose.block<3, 1>(0, 3) - (dimensions.x() / 2) * pose.block<3, 1>(0, 0) - (dimensions.y() / 2) * pose.block<3, 1>(0, 1) + (dimensions.z() / 2) * pose.block<3, 1>(0, 2));
    centers[4] = pose.block<3, 1>(0, 3) + (dimensions.z() / 2) * pose.block<3, 1>(0, 2);

    s[5].push_back(pose.block<3, 1>(0, 3) + (dimensions.x() / 2) * pose.block<3, 1>(0, 0) + (dimensions.y() / 2) * pose.block<3, 1>(0, 1) - (dimensions.z() / 2) * pose.block<3, 1>(0, 2));
    s[5].push_back(pose.block<3, 1>(0, 3) + (dimensions.x() / 2) * pose.block<3, 1>(0, 0) - (dimensions.y() / 2) * pose.block<3, 1>(0, 1) - (dimensions.z() / 2) * pose.block<3, 1>(0, 2));
    s[5].push_back(pose.block<3, 1>(0, 3) - (dimensions.x() / 2) * pose.block<3, 1>(0, 0) + (dimensions.y() / 2) * pose.block<3, 1>(0, 1) - (dimensions.z() / 2) * pose.block<3, 1>(0, 2));
    s[5].push_back(pose.block<3, 1>(0, 3) - (dimensions.x() / 2) * pose.block<3, 1>(0, 0) - (dimensions.y() / 2) * pose.block<3, 1>(0, 1) - (dimensions.z() / 2) * pose.block<3, 1>(0, 2));
    centers[5] = pose.block<3, 1>(0, 3) - (dimensions.z() / 2) * pose.block<3, 1>(0, 2);

    for (unsigned int i = 0; i < 6; i++)
    {
        sides.push_back(PlanePtr(new Plane(s[i], (centers[i] - pose.block<3, 1>(0, 3)).normalized())));
    }
}

Eigen::Matrix4f Box::getPose() const
{
    return pose;
}

Eigen::Vector3f Box::getDimensions() const
{
    return dimensions;
}

float Box::isCircular()
{
    return 0;
}

float AffordanceKit::Box::getArea() const
{
    float area = 0;
    for (auto& side : sides)
    {
        area += side->getArea();
    }
    return area;
}

template<class Archive> void AffordanceKit::Box::save(Archive& ar, const unsigned int version) const
{
    ar& boost::serialization::base_object<Primitive>(*this);

    for (unsigned int i = 0; i < 4; i++)
    {
        for (unsigned int j = 0; j < 4; j++)
        {
            ar& pose(i, j);
        }
    }

    ar& dimensions.x() & dimensions.y() & dimensions.z();
}

template<class Archive> void AffordanceKit::Box::load(Archive& ar, const unsigned int version)
{
    ar& boost::serialization::base_object<Primitive>(*this);

    for (unsigned int i = 0; i < 4; i++)
    {
        for (unsigned int j = 0; j < 4; j++)
        {
            ar& pose(i, j);
        }
    }

    float dx, dy, dz;
    ar& dx& dy& dz;

    dimensions = Eigen::Vector3f(dx, dy, dz);
    initialize();
}

BOOST_CLASS_EXPORT_IMPLEMENT(AffordanceKit::Box)
