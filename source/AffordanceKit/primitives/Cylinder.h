/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _AffordanceKit_Primitive_Cylinder_H
#define _AffordanceKit_Primitive_Cylinder_H

#include "../Primitive.h"

#include <Eigen/Eigen>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/export.hpp>

namespace AffordanceKit
{

    class Cylinder : public Primitive
    {
    public:
        Cylinder();

        /**
         * Constructs a cylinder primitive.
         *
         * basePoint: Center point of the cylinder
         * direction: Normalized axis direction
         */
        Cylinder(const Eigen::Vector3f& basePoint, const Eigen::Vector3f& direction, float length, float radius);

        Eigen::Vector3f getBasePoint();
        Eigen::Vector3f getDirection();
        float getLength();
        float getRadius();

        PrimitiveSubVolumePtr getVolumeInDirection(const Eigen::Matrix4f& pose, float depth = -1, float epsilon = 0.1) override;

        Eigen::Vector3f getCenter() const override;
        Eigen::Vector3f getDimensions() const override;
        float getArea() const override;

        float isCircular() override;

    protected:
        Eigen::MatrixXf sample_impl(float spatialStepSize, int numOrientationalSteps) const override;
        Eigen::MatrixXf sampleSlice(const Eigen::Vector3f& p, int spatialStepSize, int numOrientationalSteps) const;

    protected:
        Eigen::Vector3f basePoint;
        Eigen::Vector3f direction;
        float length;
        float radius;

    private:
        friend class boost::serialization::access;

        template<class Archive> void save(Archive& ar, const unsigned int version) const;
        template<class Archive> void load(Archive& ar, const unsigned int version);

        BOOST_SERIALIZATION_SPLIT_MEMBER()
    };

    typedef std::shared_ptr<Cylinder> CylinderPtr;
}

BOOST_CLASS_EXPORT_KEY(AffordanceKit::Cylinder)

#endif



