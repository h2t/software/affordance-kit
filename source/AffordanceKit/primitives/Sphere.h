/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _AffordanceKit_Sphere_Cylinder_H
#define _AffordanceKit_Sphere_Cylinder_H

#include "../Primitive.h"

#include <Eigen/Eigen>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/export.hpp>

namespace AffordanceKit
{
    class Sphere : public Primitive
    {
    public:
        Sphere();
        Sphere(const Eigen::Vector3f& center, float radius);

        PrimitiveSubVolumePtr getVolumeInDirection(const Eigen::Matrix4f& pose, float depth = -1, float epsilon = 0.1) override
        {
            return PrimitiveSubVolumePtr(new PrimitiveSubVolume());
        }

        Eigen::Vector3f getCenter() const override;
        Eigen::Vector3f getDimensions() const override;
        float getArea() const override;
        float getRadius();

        float isCircular() override;

    protected:
        Eigen::MatrixXf sample_impl(float spatialStepSize, int numOrientationalSteps) const override;

    protected:
        Eigen::Vector3f center;
        float radius;

    private:
        friend class boost::serialization::access;

        template<class Archive> void save(Archive& ar, const unsigned int version) const;
        template<class Archive> void load(Archive& ar, const unsigned int version);

        BOOST_SERIALIZATION_SPLIT_MEMBER()
    };

    typedef std::shared_ptr<Sphere> SpherePtr;
}

BOOST_CLASS_EXPORT_KEY(AffordanceKit::Sphere)

#endif



