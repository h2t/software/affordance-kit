/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "Plane.h"

#include <pcl/segmentation/sac_segmentation.h>

using namespace AffordanceKit;

Plane::Plane() :
    pose(Eigen::Matrix4f::Identity()),
    poseInverse(Eigen::Matrix4f::Identity()),
    circularity(0),
    samplingMinDistanceFromBorder(20)
{
}

Plane::Plane(const std::vector<Eigen::Vector3f>& convexHull, const Eigen::Vector3f& normal) :
    Plane()
{
    this->convexHull = convexHull;
    this->normal = normal;
    initialize(normal);
}

void Plane::initialize(const Eigen::Vector3f& normal)
{
    if (convexHull.size() <= 2)
    {
        std::cout << "Warning: Could not create planar primitive from " << convexHull.size() << " points." << std::endl;
        return;
    }

    computeCenter();
    computePrimitivePose(normal);
    computeCircularity();
    computeLocalCentroid();
}

const Eigen::Matrix4f& Plane::getPose() const
{
    return pose;
}

Eigen::Vector3f Plane::getNormal() const
{
    return normal;
}

const std::vector<Eigen::Vector3f>& Plane::getConvexHull() const
{
    return convexHull;
}

Eigen::MatrixXf Plane::sample_impl(float spatialStepSize, int numOrientationalSteps) const
{
    // This sampling algorithm only works for planar primitives defined by
    // a convex hull of 3D points

    std::vector<Eigen::Matrix4f> sampling;
    samplePlane(spatialStepSize, numOrientationalSteps, sampling);

    Eigen::Vector2f center = Eigen::Vector2f::Zero();
    for (unsigned int i = 0; i < localConvexHull->vertices.size(); i++)
    {
        center += localConvexHull->vertices[i];
    }
    center /= localConvexHull->vertices.size();

    // Sample the boundary of the plane (the convex hull of the observed points)
    for (auto& segment : localConvexHull->segments)
    {
        Eigen::Vector2f v1 = localConvexHull->vertices[segment.id1];
        Eigen::Vector2f v2 = localConvexHull->vertices[segment.id2];

        sampleLine(v1, v2, center, spatialStepSize, numOrientationalSteps, sampling);
    }

    // Sample the corner points
    sampleCorners(numOrientationalSteps, sampling);

    Eigen::MatrixXf S(4, sampling.size() * 4);
    for (unsigned int i = 0; i < sampling.size(); i++)
    {
        S.block<4, 4>(0, 4 * i) = sampling[i];
    }

    return S;
}

void Plane::samplePlane(float spatialStepSize, int numOrientationalSteps, std::vector<Eigen::Matrix4f>& result, bool sampleBackSide) const
{
    // Perform rejection sampling of the primitive-aligned bounding box
    // Assure that the center line (or the center point in the degenerated case) is always sampled

    if (convexHull.size() < 3)
    {
        // Not a plane
        return;
    }

    std::vector<Eigen::Vector2f> tmp_sampling;

    for (int i = (int)(dimensionsMin(0) / spatialStepSize); i <= (int)(dimensionsMax(0) / spatialStepSize); i++)
    {
        for (int j = (int)(dimensionsMin(1) / spatialStepSize); j <= (int)(dimensionsMax(1) / spatialStepSize); j++)
        {
            Eigen::Vector2f v(i * spatialStepSize, j * spatialStepSize);

            // Rejection test
            if (VirtualRobot::MathTools::isInside(v, localConvexHull))
            {
                tmp_sampling.push_back(v);
            }
        }
    }

    Eigen::Matrix4f eefPoseBack = pose;
    Eigen::Matrix4f eefPoseFront = pose;
    eefPoseFront.block<3, 3>(0, 0) *= Eigen::Matrix3f(Eigen::AngleAxisf(M_PI, Eigen::Vector3f::UnitX()));

    Eigen::Matrix4f P = Eigen::Matrix4f::Identity();

    result.reserve(tmp_sampling.size() * numOrientationalSteps * (sampleBackSide ? 2 : 1));
    for (auto& p : tmp_sampling)
    {
        P.block<4, 1>(0, 3) = pose * Eigen::Vector4f(p.x(), p.y(), 0, 1);

        // Sample front side
        for (int i = 0; i < numOrientationalSteps; i++)
        {
            P.block<3, 3>(0, 0) = eefPoseFront.block<3, 3>(0, 0) * Eigen::Matrix3f(Eigen::AngleAxisf(i * (2 * M_PI / numOrientationalSteps), Eigen::Vector3f::UnitZ()));
            result.push_back(P);
        }

        // Sample back side
        if (sampleBackSide)
        {
            for (int i = 0; i < numOrientationalSteps; i++)
            {
                P.block<3, 3>(0, 0) = eefPoseBack.block<3, 3>(0, 0) * Eigen::Matrix3f(Eigen::AngleAxisf(i * (2 * M_PI / numOrientationalSteps), Eigen::Vector3f::UnitZ()));
                result.push_back(P);
            }
        }
    }

    // Ensure that there is at least one sampling in the plane
    if (result.size() == 0)
    {
        // Use convex hull center as single sampling
        sampleCentroid(numOrientationalSteps, result);
    }
}

void Plane::sampleLine(const Eigen::Vector2f& from, const Eigen::Vector2f& to, const Eigen::Vector2f& polygonCenter, int spatialStepSize, int numOrientationalSteps, std::vector<Eigen::Matrix4f>& result) const
{
    if (convexHull.size() < 3)
    {
        // Not a plane
        return;
    }

    Eigen::Matrix4f P = Eigen::Matrix4f::Identity();

    Eigen::Vector2f direction = (to - from) / (to - from).norm();
    Eigen::Vector2f normal(-direction.y(), direction.x());

    // Let the normal point into the inside of the polygon
    if (normal.dot(polygonCenter - from) < 0)
    {
        normal = -normal;
    }

    Eigen::Vector3f globalZ = pose.block<3, 3>(0, 0) * Eigen::Vector3f(normal.x(), normal.y(), 0);
    Eigen::Vector3f globalX = pose.block<3, 3>(0, 0) * Eigen::Vector3f(direction.x(), direction.y(), 0);

    P.block<3, 1>(0, 0) = globalX / globalX.norm();
    P.block<3, 1>(0, 2) = globalZ / globalZ.norm();
    P.block<3, 1>(0, 1) = -P.block<3, 1>(0, 0).cross(P.block<3, 1>(0, 2));
    P.block<3, 1>(0, 1) = P.block<3, 1>(0, 1) / P.block<3, 1>(0, 1).norm();

    float lineLength = (to - from).norm();
    int num = (int)(lineLength / spatialStepSize);
    float initialOffset = (lineLength - (num * spatialStepSize)) / 2;

    Eigen::Matrix4f R = Eigen::Matrix4f::Identity();
    for (int i = 0; i <= num; i++)
    {
        Eigen::Vector4f p;
        p << from + (initialOffset + i * spatialStepSize) * direction, 0, 1;

        P.block<3, 1>(0, 3) = (pose * p).head(3);

        for (int i = 0; i < numOrientationalSteps; i++)
        {
            R.block<3, 3>(0, 0) = Eigen::Matrix3f(Eigen::AngleAxisf(i * (2 * M_PI / numOrientationalSteps), Eigen::Vector3f::UnitZ()));
            result.push_back(P * R);
        }
    }

    // Avoid zero samplings on a side
    if (num == 0)
    {
        Eigen::Vector4f p;
        p << from + 0.5 * (to - from), 0, 1;

        P.block<3, 1>(0, 3) = (pose * p).head(3);

        for (int i = 0; i < numOrientationalSteps; i++)
        {
            R.block<3, 3>(0, 0) = Eigen::Matrix3f(Eigen::AngleAxisf(i * (2 * M_PI / numOrientationalSteps), Eigen::Vector3f::UnitZ()));
            result.push_back(P * R);
        }
    }
}

void Plane::sampleCentroid(int numOrientationalSteps, std::vector<Eigen::Matrix4f>& result) const
{
    Eigen::Matrix4f P = this->pose;
    P.block<3, 1>(0, 3) = (pose * Eigen::Vector4f(localCentroid.x(), localCentroid.y(), 0, 1)).head(3);

    Eigen::Matrix4f R = Eigen::Matrix4f::Identity();
    for (int i = 0; i < numOrientationalSteps; i++)
    {
        R.block<3, 3>(0, 0) = Eigen::Matrix3f(Eigen::AngleAxisf(i * (2 * M_PI / numOrientationalSteps), Eigen::Vector3f::UnitZ()));
        result.push_back(P * R);
    }

    Eigen::Matrix4f T = Eigen::Matrix4f::Identity();
    T.block<3, 3>(0, 0) = Eigen::Matrix3f(Eigen::AngleAxisf(M_PI, Eigen::Vector3f::UnitX()));
    for (int i = 0; i < numOrientationalSteps; i++)
    {
        R.block<3, 3>(0, 0) = Eigen::Matrix3f(Eigen::AngleAxisf(i * (2 * M_PI / numOrientationalSteps), Eigen::Vector3f::UnitZ()));
        result.push_back(P * T * R);
    }
}

void Plane::sampleCorners(int numOrientationalSteps, std::vector<Eigen::Matrix4f>& result) const
{
    for (unsigned int i = 0; i < localConvexHull->vertices.size(); i++)
    {
        // Find connected segments
        std::vector<Eigen::Vector2f> connected;
        for (unsigned int j = 0; j < localConvexHull->segments.size(); j++)
        {
            if (localConvexHull->segments[j].id1 == (int)i)
            {
                connected.push_back(localConvexHull->vertices[localConvexHull->segments[j].id2]);
            }
            else if (localConvexHull->segments[j].id2 == (int)i)
            {
                connected.push_back(localConvexHull->vertices[localConvexHull->segments[j].id1]);
            }
        }

        if (connected.size() != 2)
        {
            // Weird convex hull => should not happen
            continue;
        }

        Eigen::Vector2f v1 = connected[0];
        Eigen::Vector2f v2 = localConvexHull->vertices[i];
        Eigen::Vector2f v3 = connected[1];
        Eigen::Vector2f n = (((v1 - v2).normalized() + (v3 - v2).normalized()) / 2).normalized();

        Eigen::Matrix4f P = Eigen::Matrix4f::Identity();
        P.block<4, 1>(0, 1) = pose.block<4, 1>(0, 2);
        P.block<4, 1>(0, 2) = pose * Eigen::Vector4f(n.x(), n.y(), 0, 0);
        P.block<4, 1>(0, 3) = pose * Eigen::Vector4f(v2.x(), v2.y(), 0, 1);
        P.block<3, 1>(0, 0) = P.block<3, 1>(0, 1).cross(P.block<3, 1>(0, 2));

        Eigen::Matrix4f R = Eigen::Matrix4f::Identity();
        for (int i = 0; i < numOrientationalSteps; i++)
        {
            R.block<3, 3>(0, 0) = Eigen::Matrix3f(Eigen::AngleAxisf(i * (2 * M_PI / numOrientationalSteps), Eigen::Vector3f::UnitZ()));
            result.push_back(P * R);
        }
    }
}

float Plane::getDistanceToBorder(const Eigen::Matrix4f& pose) const
{
    Eigen::Vector2f p = (poseInverse * pose.block<4, 1>(0, 3)).head(2);

    float dist = std::numeric_limits<float>::infinity();
    for (auto& segment : localConvexHull->segments)
    {
        Eigen::Vector2f v1 = localConvexHull->vertices[segment.id1];
        Eigen::Vector2f v2 = localConvexHull->vertices[segment.id2];

        dist = std::min(dist, VirtualRobot::MathTools::distPointSegment(v1, v2, p));
    }

    return dist;
}

bool Plane::isInPlane(const Eigen::Vector3f& p, float epsilon) const
{
    Eigen::Vector4f v = poseInverse * Eigen::Vector4f(p.x(), p.y(), p.z(), 1);

    return (fabs(v.z()) < epsilon);
}

void Plane::computeCircularity()
{
    unsigned int maxIterations = 100;
    float distanceThreshold = 10;

    // Convert the convex hull to a PCL point cloud
    bool first = true;
    pcl::PointCloud<pcl::PointXYZ>::Ptr localConvexHullPointCloud(new pcl::PointCloud<pcl::PointXYZ>);
    unsigned int size = 0;
    for (auto& s : localConvexHull->segments)
    {
        Eigen::Vector4f v1;
        v1 << localConvexHull->vertices[s.id1], 0, 1;

        Eigen::Vector4f v2;
        v2 << localConvexHull->vertices[s.id2], 0, 1;

        v1 = pose * v1;
        v2 = pose * v2;

        if (first)
        {
            localConvexHullPointCloud->push_back(pcl::PointXYZ(v1.x(), v1.y(), v1.z()));
            size++;
            first = false;
        }

        // Add intermediate points to the point cloud in order to enforce a better fitting of the circular model
        for (float factor :
             {
                 0.33, 0.66, 1.0
             })
        {
            Eigen::Vector4f v = v1 + factor * (v2 - v1);
            localConvexHullPointCloud->push_back(pcl::PointXYZ(v.x(), v.y(), v.z()));
            size++;
        }

        localConvexHullPointCloud->push_back(pcl::PointXYZ(v2.x(), v2.y(), v2.z()));
        size++;
    }

    pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers(new pcl::PointIndices);

    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> segmentation;
    segmentation.setOptimizeCoefficients(true);
    segmentation.setModelType(pcl::SACMODEL_CIRCLE3D);
    segmentation.setMethodType(pcl::SAC_RANSAC);
    segmentation.setDistanceThreshold(distanceThreshold);
    segmentation.setMaxIterations(maxIterations);
    segmentation.setInputCloud(localConvexHullPointCloud);
    segmentation.segment(*inliers, *coefficients);

    circularity = 1.0 * inliers->indices.size() / size;
}

PrimitiveSubVolumePtr Plane::getVolumeInDirection(const Eigen::Matrix4f& pose, float depth, float epsilon)
{
    if (localConvexHull->segments.size() == 0)
    {
        return PrimitiveSubVolumePtr(new PrimitiveSubVolume());
    }

    Eigen::Matrix4f P_inv = pose.inverse();

    std::vector<Eigen::Vector3f> points;
    points.reserve(localConvexHull->vertices.size());
    for (unsigned int i = 0; i < localConvexHull->vertices.size(); i++)
    {
        Eigen::Vector2f v = localConvexHull->vertices[i];
        points.push_back((P_inv * this->pose * Eigen::Vector4f(v.x(), v.y(), 0, 1)).head(3));
    }

    float d_x_min, d_x_max, d_y_min, d_y_max, d_z_max;
    d_x_min = d_x_max = d_y_min = d_y_max = d_z_max = 0;

    std::vector<Eigen::Vector3f> crossingPoints;
    for (auto& segment : localConvexHull->segments)
    {
        Eigen::Vector3f v1 = points[segment.id1];
        Eigen::Vector3f v2 = points[segment.id2];

        if (depth >= 0 && ((v1.z() > depth + epsilon && v2.z() > depth + epsilon) || (v1.z() < -epsilon && v2.z() < -epsilon)))
        {
            // Both points are outside depth range
            continue;
        }
        else if (depth >= 0 && v2.z() > depth + epsilon)
        {
            // Only v2 is outside depth range
            v2 = v1 + ((depth - v1.z()) / (v2.z() - v1.z())) * (v2 - v1);
        }
        else if (depth >= 0 && v1.z() > depth + epsilon)
        {
            // Only v1 is outside depth range
            v1 = v2 + ((depth - v2.z()) / (v1.z() - v2.z())) * (v1 - v2);
        }

        if (v1.z() < -epsilon)
        {
            v1 = v2 + (v2.z() / (v1.z() - v2.z())) * (v1 - v2);
        }

        if (v2.z() < -epsilon)
        {
            v2 = v1 + (v1.z() / (v2.z() - v1.z())) * (v2 - v1);
        }

        // TODO: The volume will not be correct in z-direction
        //       In order to fix this, segments between clamped points would need to be added
        //       in the case when both points are outside depth range
        //d_z_max = std::max(d_z_max, std::max(v1.z(), v2.z()));

        v1(2) = 0;
        v2(2) = 0;

        // Determine crossing points (i.e. points on the boundary of the primitive that cross the local x and y axes)
        if (v1.x() * v2.x() < epsilon)
        {
            if (fabs(v1.x()) < epsilon && fabs(v2.x()) < epsilon)
            {
                crossingPoints.push_back(Eigen::Vector3f(0, 0, 0));
                crossingPoints.push_back(v1);
                crossingPoints.push_back(v2);
            }
            else
            {
                crossingPoints.push_back(v1 + (0 - v1.x()) / (v2.x() - v1.x()) * (v2 - v1));
            }
        }
        if (v1.y() * v2.y() < epsilon)
        {
            if (fabs(v1.y()) < epsilon && fabs(v2.y()) < epsilon)
            {
                crossingPoints.push_back(Eigen::Vector3f(0, 0, 0));
                crossingPoints.push_back(v1);
                crossingPoints.push_back(v2);
            }
            else
            {
                crossingPoints.push_back(v1 + (0 - v1.y()) / (v2.y() - v1.y()) * (v2 - v1));
            }
        }
    }

    for (auto& cp : crossingPoints)
    {
        d_x_min = std::min(d_x_min, cp.x());
        d_x_max = std::max(d_x_max, cp.x());
        d_y_min = std::min(d_y_min, cp.y());
        d_y_max = std::max(d_y_max, cp.y());
    }

    return PrimitiveSubVolumePtr(new PrimitiveSubVolume(d_x_min, d_x_max, d_y_min, d_y_max, 0, d_z_max));
}

Eigen::Vector3f Plane::getCenter() const
{
    return center;
}

Eigen::Vector3f Plane::getDimensions() const
{
    return dimensions;
}

float Plane::isCircular()
{
    return circularity;
}

void Plane::computePrimitivePose(const Eigen::Vector3f& normal)
{
    if (convexHull.size() < 3)
    {
        return;
    }

    Eigen::Vector3f n;
    if (!normal.isZero(1e-4))
    {
        n = normal;
    }
    else
    {
        // Automatically determine normal (using PCA)
        Eigen::MatrixXf M(convexHull.size(), 3);

        for (unsigned int i = 0; i < convexHull.size(); i++)
        {
            M.block<1, 3>(i, 0) = (convexHull[i] - center).transpose();
        }

        Eigen::MatrixXf C = (1.0 / (convexHull.size() - 1)) * M.transpose() * M;
        Eigen::EigenSolver<Eigen::MatrixXf> eig(C);

        float ev1 = fabs(eig.eigenvalues().real()[0]);
        float ev2 = fabs(eig.eigenvalues().real()[1]);
        float ev3 = fabs(eig.eigenvalues().real()[2]);

        Eigen::Vector3f evec1 = eig.eigenvectors().col(0).real();
        Eigen::Vector3f evec2 = eig.eigenvectors().col(1).real();
        Eigen::Vector3f evec3 = eig.eigenvectors().col(2).real();

        n = (ev1 <= ev2 && ev1 <= ev3) ? evec1 : ((ev2 <= ev1 && ev2 <= ev3) ? evec2 : evec3);
    }

    // Use aribtrary (but suitable) vectors for the temporary primitive pose
    Eigen::Vector3f v1 = (convexHull[0] - center).normalized();
    Eigen::Vector3f v2 = v1.cross(n);

    if (v1.cross(v2).dot(n) < 0)
    {
        // Fix handedness of primitive coordinate system
        v1 *= -1;
    }

    pose = Eigen::Matrix4f::Identity();
    pose.block<3, 1>(0, 0) = v1;
    pose.block<3, 1>(0, 1) = v2;
    pose.block<3, 1>(0, 2) = n;
    pose.block<3, 1>(0, 3) = center;
    poseInverse = pose.inverse();

    // Compute local convex hull for the temporary primitive pose
    computeLocalConvexHull();

    // Determine base vectors in a way that minimizes the resulting bounding box of the plane
    float minArea = std::numeric_limits<float>::infinity();
    Eigen::Vector2f new_v1 = Eigen::Vector2f::UnitX();
    Eigen::Vector2f new_v2 = Eigen::Vector2f::UnitY();

    for (auto& segment : localConvexHull->segments)
    {
        Eigen::Vector2f p1 = localConvexHull->vertices[segment.id1];
        Eigen::Vector2f p2 = localConvexHull->vertices[segment.id2];

        Eigen::Vector2f dir1 = (p1 - p2).normalized();
        Eigen::Vector2f dir2(dir1.y(), -dir1.x());

        Eigen::Vector2f max(-std::numeric_limits<float>::infinity(), -std::numeric_limits<float>::infinity());
        Eigen::Vector2f min(std::numeric_limits<float>::infinity(), std::numeric_limits<float>::infinity());

        for (auto& p : localConvexHull->vertices)
        {
            float d1 = p.dot(dir1);
            float d2 = p.dot(dir2);

            max(0) = std::max(max(0), d1);
            max(1) = std::max(max(1), d2);

            min(0) = std::min(min(0), d1);
            min(1) = std::min(min(1), d2);
        }

        float a = (max(0) - min(0)) * (max(1) - min(1));
        if (a < minArea)
        {
            minArea = a;

            // Sort axes by bounding box extent
            if (max(0) - min(0) > max(1) - min(1))
            {
                dimensions = Eigen::Vector3f(max(0) - min(0), max(1) - min(1), 0);
                dimensionsMin = Eigen::Vector3f(min(0), min(1), 0);
                dimensionsMax = Eigen::Vector3f(max(0), max(1), 0);
                new_v1 = dir1;
                new_v2 = dir2;
            }
            else
            {
                dimensions = Eigen::Vector3f(max(1) - min(1), max(0) - min(0), 0);
                dimensionsMin = Eigen::Vector3f(min(1), min(0), 0);
                dimensionsMax = Eigen::Vector3f(max(1), max(0), 0);
                new_v1 = dir2;
                new_v2 = dir1;
            }
        }
    }

    // Compute new (permanent) primitive pose
    v1 = pose.block<3, 3>(0, 0) * Eigen::Vector3f(new_v1.x(), new_v1.y(), 0);
    v2 = pose.block<3, 3>(0, 0) * Eigen::Vector3f(new_v2.x(), new_v2.y(), 0);

    if (v1.cross(v2).dot(n) < 0)
    {
        // Fix handedness of primitive coordinate system
        v1 *= -1;

        float tmp = -dimensionsMin(0);
        dimensionsMin(0) = -dimensionsMax(0);
        dimensionsMax(0) = tmp;
    }

    pose = Eigen::Matrix4f::Identity();
    pose.block<3, 1>(0, 0) = v1;
    pose.block<3, 1>(0, 1) = v2;
    pose.block<3, 1>(0, 2) = n;
    pose.block<3, 1>(0, 3) = center;
    poseInverse = pose.inverse();

    this->normal = n;

    // Re-compute local convex hull
    computeLocalConvexHull();
}

void Plane::computeLocalConvexHull()
{
    std::vector<Eigen::Vector2f> ch;

    ch.resize(convexHull.size());
    for (unsigned int i = 0; i < convexHull.size(); i++)
    {
        Eigen::Vector3f v = convexHull[i];
        ch[i] = (poseInverse * Eigen::Vector4f(v.x(), v.y(), v.z(), 1)).head(2);
    }

    localConvexHull = VirtualRobot::MathTools::createConvexHull2D(ch);
}

void Plane::computeCenter()
{
    center = Eigen::Vector3f::Zero();

    for (auto v : convexHull)
    {
        center += v;
    }

    center /= convexHull.size();
}

void Plane::computeLocalCentroid()
{
    localCentroid = Eigen::Vector2f::Zero();

    std::vector<Eigen::Vector2f> sortedPoints;
    for (unsigned int i = 0; i < localConvexHull->segments.size(); i++)
    {
        if (i == 0)
        {
            sortedPoints.push_back(localConvexHull->vertices[localConvexHull->segments[i].id1]);
        }
        sortedPoints.push_back(localConvexHull->vertices[localConvexHull->segments[i].id2]);
    }

    float area = 0;
    for (unsigned int i = 0; i < sortedPoints.size(); i++)
    {
        Eigen::Vector2f p0 = sortedPoints[i];
        Eigen::Vector2f p1 = sortedPoints[(i + 1) % sortedPoints.size()];

        float a = p0.x() * p1.y() - p1.x() * p0.y();
        area += a;

        localCentroid += a * (p0 + p1);
    }

    localCentroid /= 6 * area / 2;
}

float AffordanceKit::Plane::getArea() const
{
    float area = 0;

    for (auto& segment : localConvexHull->segments)
    {
        Eigen::Vector2f v1 = localConvexHull->vertices[segment.id1];
        Eigen::Vector2f v2 = localConvexHull->vertices[segment.id2];

        area += v1.x() * v2.y() - v1.y() * v2.x();
    }

    return 0.5 * fabs(area);
}

template<class Archive> void AffordanceKit::Plane::save(Archive& ar, const unsigned int version) const
{
    ar& boost::serialization::base_object<Primitive>(*this);

    unsigned int sz = convexHull.size();
    ar& normal.x() & normal.y() & normal.z() & sz;

    for (auto& p : convexHull)
    {
        ar& p.x() & p.y() & p.z();
    }
}

template<class Archive> void AffordanceKit::Plane::load(Archive& ar, const unsigned int version)
{
    ar& boost::serialization::base_object<Primitive>(*this);

    unsigned int sz;
    float nx, ny, nz;
    ar& nx& ny& nz& sz;

    std::vector<Eigen::Vector3f> ch;
    for (unsigned int i = 0; i < sz; i++)
    {
        float px, py, pz;
        ar& px& py& pz;
        convexHull.push_back(Eigen::Vector3f(px, py, pz));
    }

    normal = Eigen::Vector3f(nx, ny, nz);
    initialize(normal);
}

BOOST_CLASS_EXPORT_IMPLEMENT(AffordanceKit::Plane)
