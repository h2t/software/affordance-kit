/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _AffordanceKit_Primitive_Plane_H
#define _AffordanceKit_Primitive_Plane_H

#include "../Primitive.h"

#include <Eigen/Eigen>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/split_member.hpp>

namespace AffordanceKit
{
    class Box;

    class Plane : public Primitive
    {
        friend Box;

    public:
        Plane();
        Plane(const std::vector<Eigen::Vector3f>& convexHull, const Eigen::Vector3f& normal = Eigen::Vector3f::Zero());

        const Eigen::Matrix4f& getPose() const;
        Eigen::Vector3f getNormal() const;
        const std::vector<Eigen::Vector3f>& getConvexHull() const;

        PrimitiveSubVolumePtr getVolumeInDirection(const Eigen::Matrix4f& pose, float depth = -1, float epsilon = 0.1) override;
        Eigen::Vector3f getCenter() const override;
        Eigen::Vector3f getDimensions() const override;
        Eigen::Vector3f getBoundingBoxMin() const
        {
            return dimensionsMin;
        }
        Eigen::Vector3f getBoundingBoxMax() const
        {
            return dimensionsMax;
        }
        float getArea() const override;

        float isCircular() override;

        float getDistanceToBorder(const Eigen::Matrix4f& pose) const;
        bool isInPlane(const Eigen::Vector3f& p, float epsilon = 1e-2) const;

    protected:
        void computePrimitivePose(const Eigen::Vector3f& normal = Eigen::Vector3f::Zero());
        void computeLocalConvexHull();
        void computeCenter();
        void computeLocalCentroid();

        Eigen::MatrixXf sample_impl(float spatialStepSize, int numOrientationalSteps) const override;
        void samplePlane(float spatialStepSize, int numOrientationalSteps, std::vector<Eigen::Matrix4f>& result, bool sampleBackSide = true) const;
        void sampleLine(const Eigen::Vector2f& from, const Eigen::Vector2f& to, const Eigen::Vector2f& polygonCenter, int spatialStepSize, int numOrientationalSteps, std::vector<Eigen::Matrix4f>& result) const;
        void sampleCentroid(int numOrientationalSteps, std::vector<Eigen::Matrix4f>& result) const;
        void sampleCorners(int numOrientationalSteps, std::vector<Eigen::Matrix4f>& result) const;

        void computeCircularity();

        void initialize(const Eigen::Vector3f& normal = Eigen::Vector3f::Zero());

    protected:
        Eigen::Matrix4f pose;
        Eigen::Matrix4f poseInverse;
        Eigen::Vector3f center;
        Eigen::Vector2f localCentroid;
        Eigen::Vector3f dimensions;
        Eigen::Vector3f dimensionsMin;
        Eigen::Vector3f dimensionsMax;
        Eigen::Vector3f normal;

        std::vector<Eigen::Vector3f> convexHull;
        VirtualRobot::MathTools::ConvexHull2DPtr localConvexHull;

        float circularity;

        const float samplingMinDistanceFromBorder;

    private:
        friend class boost::serialization::access;

        template<class Archive> void save(Archive& ar, const unsigned int version) const;
        template<class Archive> void load(Archive& ar, const unsigned int version);

        BOOST_SERIALIZATION_SPLIT_MEMBER()
    };

    typedef std::shared_ptr<Plane> PlanePtr;
}

BOOST_CLASS_EXPORT_KEY(AffordanceKit::Plane)

#endif


