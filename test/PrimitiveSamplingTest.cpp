/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "../source/AffordanceKit/primitives/Plane.h"

#define BOOST_TEST_MODULE PrimitiveSamplingTest
#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

BOOST_AUTO_TEST_CASE(PrimitiveSamplingTestCase1)
{
    // Toy Example 1: Triangle
    std::vector<Eigen::Vector3f> convexHull;
    convexHull.push_back(Eigen::Vector3f(0, 0, 0));
    convexHull.push_back(Eigen::Vector3f(8.5, 0, 0));
    convexHull.push_back(Eigen::Vector3f(8.5, 4.5, 0));
    AffordanceKit::PlanePtr plane(new AffordanceKit::Plane(convexHull));

    std::vector<Eigen::Vector3f> expectedSamplingPositions;
    std::vector<Eigen::Vector3f> expectedSamplingDirections;

    // Bottom side
    for (int i = 0; i <= 8; i++)
    {
        expectedSamplingPositions.push_back(Eigen::Vector3f(i + 0.25, 0, 0));
        expectedSamplingDirections.push_back(Eigen::Vector3f(0, 1, 0));
    }

    // Right side
    for (int i = 0; i <= 4; i++)
    {
        expectedSamplingPositions.push_back(Eigen::Vector3f(8.5, i + 0.25, 0));
        expectedSamplingDirections.push_back(Eigen::Vector3f(-1, 0, 0));
    }

    // Top side
    Eigen::Vector3f d(8.5 / sqrt(92.5), 4.5 / sqrt(92.5), 0);
    for (int i = 0; i <= 9; i++)
    {
        expectedSamplingPositions.push_back(((sqrt(92.5) - 9) / 2 + i) * d);
        expectedSamplingDirections.push_back(Eigen::Vector3f(4.5 / sqrt(92.5), -8.5 / sqrt(92.5), 0));
    }

    // Corners
    expectedSamplingPositions.push_back(Eigen::Vector3f(0, 0, 0));
    expectedSamplingDirections.push_back(Eigen::Vector3f(0.97, 0.24, 0));
    expectedSamplingPositions.push_back(Eigen::Vector3f(8.5, 0, 0));
    expectedSamplingDirections.push_back(Eigen::Vector3f(-1 / sqrt(2), 1 / sqrt(2), 0));
    expectedSamplingPositions.push_back(Eigen::Vector3f(8.5, 4.5, 0));
    expectedSamplingDirections.push_back(Eigen::Vector3f(-0.52, -0.86, 0));

    plane->sample(1, 1);
    BOOST_CHECK(plane->getSamplingSize() > expectedSamplingPositions.size());

    int count = 0;
    for (unsigned int i = 0; i < expectedSamplingPositions.size(); i++)
    {
        for (unsigned int j = 0; j < plane->getSamplingSize(); j++)
        {
            Eigen::Matrix4f s = plane->getSampling(j);
            if ((s.block<3, 1>(0, 3) - expectedSamplingPositions.at(i)).norm() < 0.1 && (s.block<3, 1>(0, 2) - expectedSamplingDirections.at(i)).norm() < 0.1)
            {
                count++;
                break;
            }
        }
    }
    BOOST_CHECK_EQUAL(count, expectedSamplingPositions.size());
}
