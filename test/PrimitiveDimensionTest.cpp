/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "../source/AffordanceKit/primitives/Plane.h"

#define BOOST_TEST_MODULE PrimitiveSamplingTest
#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

const float tolerance = 1e-4;

BOOST_AUTO_TEST_CASE(PrimitiveSamplingTestCase1)
{
    // Example 1: Triangle
    std::vector<Eigen::Vector3f> convexHull;
    convexHull.push_back(Eigen::Vector3f(0, 0, 0));
    convexHull.push_back(Eigen::Vector3f(8.5, 0, 0));
    convexHull.push_back(Eigen::Vector3f(8.5, 4.5, 0));
    AffordanceKit::PlanePtr plane(new AffordanceKit::Plane(convexHull));

    BOOST_CHECK_CLOSE(plane->getDimensions()(0), 8.5, tolerance);
    BOOST_CHECK_CLOSE(plane->getDimensions()(1), 4.5, tolerance);
    BOOST_CHECK_CLOSE(plane->getDimensions()(2), 0, tolerance);
}

