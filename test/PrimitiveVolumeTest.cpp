/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "../source/AffordanceKit/primitives/Plane.h"

#define BOOST_TEST_MODULE PrimitiveVolumeTest
#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

const float tolerance = 0.1;

BOOST_AUTO_TEST_CASE(PrimitiveVolumeTestCase1)
{
    // Toy Example 1: Triangle
    std::vector<Eigen::Vector3f> convexHull;
    convexHull.push_back(Eigen::Vector3f(0, 0, 0));
    convexHull.push_back(Eigen::Vector3f(7, 0, 0));
    convexHull.push_back(Eigen::Vector3f(5, 0, 5));
    AffordanceKit::PlanePtr plane(new AffordanceKit::Plane(convexHull));

    Eigen::Matrix4f pose;
    pose <<  1, 0,  0,  2,
         0, 1,  0,  0,
         0, 0,  1,  0,
         0, 0,  0,  1;
    AffordanceKit::PrimitiveSubVolumePtr v = plane->getVolumeInDirection(pose, 3);

    BOOST_CHECK_CLOSE(v->getBalancedLength().x(), 4, tolerance);
    BOOST_CHECK_SMALL(v->getBalancedLength().y(), tolerance);
    BOOST_CHECK_SMALL(v->getBalancedLength().z(), tolerance);

    BOOST_CHECK_CLOSE(v->getLength().x(), 7, tolerance);
    BOOST_CHECK_SMALL(v->getLength().y(), tolerance);

    // z-distance is not properly implemented at the moment
    //BOOST_CHECK_CLOSE(v->getLength().z(), 3, tolerance);

    pose << -1, 0,  0,  4,
         0, 0, -1,  0,
         0, 1,  0,  2,
         0, 0,  0,  1;
    v = plane->getVolumeInDirection(pose, 3);

    BOOST_CHECK_CLOSE(v->getBalancedLength().x(), 4, tolerance);
    BOOST_CHECK_CLOSE(v->getBalancedLength().y(), 4, tolerance);
    BOOST_CHECK_SMALL(v->getBalancedLength().z(), tolerance);

    BOOST_CHECK_CLOSE(v->getLength().x(), 4.2, tolerance);
    BOOST_CHECK_CLOSE(v->getLength().y(), 4, tolerance);
    BOOST_CHECK_SMALL(v->getLength().z(), tolerance);
}

BOOST_AUTO_TEST_CASE(PrimitiveVolumeTestCase2)
{
    // Toy Example 2: More complex shape
    std::vector<Eigen::Vector3f> convexHull;
    convexHull.push_back(Eigen::Vector3f(0, 0, 0));
    convexHull.push_back(Eigen::Vector3f(4, 4, 0));
    convexHull.push_back(Eigen::Vector3f(9, 4, 0));
    convexHull.push_back(Eigen::Vector3f(12, 1, 0));
    convexHull.push_back(Eigen::Vector3f(12, 0, 0));
    convexHull.push_back(Eigen::Vector3f(5, -6, 0));
    convexHull.push_back(Eigen::Vector3f(0, -4, 0));
    AffordanceKit::PlanePtr plane(new AffordanceKit::Plane(convexHull));

    Eigen::Matrix4f pose;
    pose <<  1, 0,  0,  4,
         0, 1,  0,  1,
         0, 0,  1,  0,
         0, 0,  0,  1;
    AffordanceKit::PrimitiveSubVolumePtr v = plane->getVolumeInDirection(pose, 2);

    BOOST_CHECK_CLOSE(v->getBalancedLength().x(), 6, tolerance);
    BOOST_CHECK_CLOSE(v->getBalancedLength().y(), 6, tolerance);
    BOOST_CHECK_SMALL(v->getBalancedLength().z(), tolerance);

    BOOST_CHECK_CLOSE(v->getLength().x(), 11, tolerance);
    BOOST_CHECK_CLOSE(v->getLength().y(), 9, tolerance);
    BOOST_CHECK_SMALL(v->getLength().z(), tolerance);

    pose <<  1 / sqrt(2), 0,  1 / sqrt(2),  2,
         1 / sqrt(2), 0, -1 / sqrt(2),  2,
         0,        -1,  0,         0,
         0,         0,  0,          1;
    v = plane->getVolumeInDirection(pose, 2);

    BOOST_CHECK_CLOSE(v->getBalancedLength().x(), sqrt(32) + 4, tolerance);
    BOOST_CHECK_SMALL(v->getBalancedLength().y(), tolerance);
    BOOST_CHECK_SMALL(v->getBalancedLength().z(), tolerance);

    BOOST_CHECK_CLOSE(v->getLength().x(), sqrt(32) + 4, tolerance);
    BOOST_CHECK_SMALL(v->getLength().y(), tolerance);

    // z-distance is not properly implemented at the moment
    //BOOST_CHECK_CLOSE(v->getLength().z(), 2, tolerance);

    pose <<  -1 / sqrt(2), 0, -1 / sqrt(2),  10,
         1 / sqrt(2), 0, -1 / sqrt(2),  3,
         0,         1,  0,         0,
         0,         0,  0,          1;
    v = plane->getVolumeInDirection(pose, 2);

    BOOST_CHECK_CLOSE(v->getBalancedLength().x(), 2 * (2 + sqrt(2)), tolerance);
    BOOST_CHECK_SMALL(v->getBalancedLength().y(), tolerance);
    BOOST_CHECK_SMALL(v->getBalancedLength().z(), tolerance);

    BOOST_CHECK_CLOSE(v->getLength().x(), 2 + 3 * sqrt(2) + 1 / sqrt(2), tolerance);
    BOOST_CHECK_SMALL(v->getLength().y(), tolerance);

    // z-distance is not properly implemented at the moment
    //BOOST_CHECK_CLOSE(v->getLength().z(), 2, tolerance);
}

BOOST_AUTO_TEST_CASE(PrimitiveVolumeTestCase3)
{
    // Realistic test case
    std::vector<Eigen::Vector3f> convexHull;
    convexHull.push_back(Eigen::Vector3f(-116.072, 424.674, 960.851));
    convexHull.push_back(Eigen::Vector3f(-117.058, 425.269, 960.412));
    convexHull.push_back(Eigen::Vector3f(-121.391, 434.278, 946.177));
    convexHull.push_back(Eigen::Vector3f(-94.1681, 443.381, 909.182));
    convexHull.push_back(Eigen::Vector3f(-93.173, 443.089, 909.031));
    convexHull.push_back(Eigen::Vector3f(8.14466, 399.471, 920.45));
    convexHull.push_back(Eigen::Vector3f(-9.86944, 390.27, 951.044));
    convexHull.push_back(Eigen::Vector3f(-12.0244, 389.817, 953.459));
    convexHull.push_back(Eigen::Vector3f(-17.6508, 390.961, 955.283));
    AffordanceKit::PlanePtr plane(new AffordanceKit::Plane(convexHull));

    Eigen::Matrix4f pose;
    pose <<  0.249067, -0.312996,  0.916515,    -121.4,
         -0.518053, -0.842626, -0.146979,   434.279,
         0.818282, -0.438196, -0.372019,    946.18,
         0,         0,         0,         1;
    AffordanceKit::PrimitiveSubVolumePtr v = plane->getVolumeInDirection(pose, 0);

    BOOST_CHECK_SMALL(v->getLength().y(), tolerance);
}
