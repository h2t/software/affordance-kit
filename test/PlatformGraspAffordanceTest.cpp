/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "../source/AffordanceKit/primitives/Plane.h"
#include "../source/AffordanceKit/affordances/PlatformGraspAffordance.h"
#include "../source/AffordanceKit/Embodiment.h"

#define BOOST_TEST_MODULE PlatformGraspAffordanceTest
#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

float getSubVolumeEdgeLengthSum(const AffordanceKit::PrimitiveSubVolumePtr& v)
{
    return v->getLength().sum();
}

BOOST_AUTO_TEST_CASE(PlatformGraspAffordanceTestCase1)
{
    std::vector<Eigen::Vector3f> convexHull;
    convexHull.push_back(Eigen::Vector3f(-714.07, 1926.16, 1662.11));
    convexHull.push_back(Eigen::Vector3f(-727.107, 1957.25, 1702.84));
    convexHull.push_back(Eigen::Vector3f(-724.918, 1953.45, 1721.68));
    convexHull.push_back(Eigen::Vector3f(-610.647, 1705.78, 1812.88));
    convexHull.push_back(Eigen::Vector3f(-607.729, 1699.38, 1813.76));
    convexHull.push_back(Eigen::Vector3f(-597.673, 1677.1, 1813.02));
    convexHull.push_back(Eigen::Vector3f(-594.1, 1669.05, 1810.48));
    convexHull.push_back(Eigen::Vector3f(-617.695, 1705.25, 1522.18));
    convexHull.push_back(Eigen::Vector3f(-618.188, 1706.1, 1517.89));
    convexHull.push_back(Eigen::Vector3f(-625.863, 1720.18, 1465.55));
    convexHull.push_back(Eigen::Vector3f(-626.452, 1721.34, 1463.09));
    convexHull.push_back(Eigen::Vector3f(-627.102, 1722.76, 1462.74));
    convexHull.push_back(Eigen::Vector3f(-627.526, 1723.69, 1462.69));
    convexHull.push_back(Eigen::Vector3f(-636.776, 1745.14, 1480.52));
    AffordanceKit::PlanePtr plane(new AffordanceKit::Plane(convexHull));

    float d = 200;
    AffordanceKit::EmbodimentPtr embodiment(new AffordanceKit::Embodiment(d, d, 0, 0, 0, 0, 0));
    AffordanceKit::PlatformGraspAffordancePtr affordance(new AffordanceKit::PlatformGraspAffordance(embodiment));

    plane->sample(50, 4);
    affordance->evaluatePrimitive(plane);

    // First test: All computed subvolumes should be valid
    for (unsigned int i = 0; i < plane->getSamplingSize(); i++)
    {
        BOOST_CHECK(plane->getVolumeInDirection(plane->getSampling(i), 0)->isValid());
        BOOST_CHECK(plane->getVolumeInDirection(plane->getSampling(i), 10)->isValid());
        BOOST_CHECK(plane->getVolumeInDirection(plane->getSampling(i), 50)->isValid());
        BOOST_CHECK(plane->getVolumeInDirection(plane->getSampling(i), 1000)->isValid());
    }

    // Second test: The computed subvolumes should have reasonable dimensions
    float maxEdgeLengthSum = 1000;
    for (unsigned int i = 0; i < plane->getSamplingSize(); i++)
    {
        BOOST_CHECK(getSubVolumeEdgeLengthSum(plane->getVolumeInDirection(plane->getSampling(i), 0)) < maxEdgeLengthSum);
        BOOST_CHECK(getSubVolumeEdgeLengthSum(plane->getVolumeInDirection(plane->getSampling(i), 10)) < maxEdgeLengthSum);
        BOOST_CHECK(getSubVolumeEdgeLengthSum(plane->getVolumeInDirection(plane->getSampling(i), 50)) < maxEdgeLengthSum);
        BOOST_CHECK(getSubVolumeEdgeLengthSum(plane->getVolumeInDirection(plane->getSampling(i), 1000)) < maxEdgeLengthSum);
    }

    // Third test: None of the samplings with significant expected probability should be closer to the border than 200mm
    for (unsigned int i = 0; i < plane->getSamplingSize(); i++)
    {
        AffordanceKit::Belief ee = affordance->evaluateTheta(plane, i);

        // Check for 90% of the minimum distance (we tolerate a certain degree of error due to our imprecise implementation)
        BOOST_CHECK(ee.expectedProbability() < 0.1 || plane->getDistanceToBorder(plane->getSampling(i)) > 0.90 * (d / 2));

        // Check for 110% of the maximum distance (we tolerate a certain degree of error due to our imprecise implementation)
        BOOST_CHECK(ee.expectedProbability() > 0.1 || plane->getDistanceToBorder(plane->getSampling(i)) < 1.10 * (d / 2));
    }
}
