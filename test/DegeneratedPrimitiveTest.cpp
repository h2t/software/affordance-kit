/*
 * This file is part of AffordanceKit.
 *
 * AffordanceKit is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * AffordanceKit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKit
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "../source/AffordanceKit/primitives/Plane.h"

#define BOOST_TEST_MODULE PrimitiveSamplingTest
#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

BOOST_AUTO_TEST_CASE(PrimitiveSamplingTestCase1)
{
    // Plane from one point
    std::vector<Eigen::Vector3f> convexHull1;
    convexHull1.push_back(Eigen::Vector3f(0, 0, 0));
    AffordanceKit::PlanePtr plane1(new AffordanceKit::Plane(convexHull1));
    plane1->sample(1, 4);
    BOOST_CHECK_EQUAL(plane1->getSamplingSize(), 0);

    // Plane from two points
    std::vector<Eigen::Vector3f> convexHull2;
    convexHull2.push_back(Eigen::Vector3f(0, 0, 0));
    convexHull2.push_back(Eigen::Vector3f(1, 0, 0));
    AffordanceKit::PlanePtr plane2(new AffordanceKit::Plane(convexHull2));
    BOOST_CHECK_EQUAL(plane2->getSamplingSize(), 0);
}


